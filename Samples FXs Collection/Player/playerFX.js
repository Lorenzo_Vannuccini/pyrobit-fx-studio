/*
    This file is part of PyroBit FX Studio,
    � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
      
    PyroBit FX Studio is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    PyroBit FX Studio is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

// TODO: massive refactoring

var scenes = [];
var __playerID = 0;
var __playerOnFocusID = -1;
var fullscreenSceneID = -1;
var fullscreenScene = null;

function hideSceneThumbnail(sceneID)
{
    var thumbnail = getElement("thumb_" + sceneID);
    thumbnail.style.opacity = 0.0;
}

function updateSceneState(sceneID)
{
    var sceneFX = scenes[sceneID];
     
    var isPlaying = sceneFX.running;
    var playingBtn = getElement("play_" + sceneID);
    playingBtn.src = (isPlaying ? "Player/Res/pause.png" : "Player/Res/play.png");
    
    hideSceneThumbnail(sceneID);
}

function playPauseScene(sceneID)
{
    var sceneFX = scenes[sceneID];
    
    if(!sceneFX.running)
    {
        resetAllScenes();
        sceneFX.resume();
        
    } else sceneFX.pause();
    
    updateSceneState(sceneID);
}

function clearFullscreenScene(sceneID)
{   
    if(fullscreenScene != null)
    {
        fullscreenScene.free();
        fullscreenScene = null;
        fullscreenSceneID = -1;
    }

    var fullscreenIcon = getElement("fullscrn_" + sceneID);
    fullscreenIcon.style.zIndex  = -1.0;
    fullscreenIcon.style.opacity = 0.0;
}

function runFullscreenScene(sceneID,fullscreen)
{   
    var sceneFX = scenes[sceneID];
    clearFullscreenScene(sceneID);
    
    if(fullscreen)
    {
        resetAllScenes();
        fullscreenSceneID = sceneID;
        
        fullscreenScene = new SceneFX();
        fullscreenScene.pause();
        
        fullscreenScene.setSimulation( function(thisScene,dt) {
            var cursorSceneCoords = thisScene.screenToClient(cursorAbsoluteX,cursorAbsoluteY);
            thisScene.setProgramData("target", cursorSceneCoords);
        });       
        
        sceneFX.emitters.every( function(emitterSrc)
        {          
            var emitterDst = fullscreenScene.createEmitterCopy(emitterSrc);
            
            emitterDst.pos.y = fullscreenScene.screenToClient(0.0,cursorAbsoluteY - 125.0).y;
            emitterDst.pos.x = fullscreenScene.getWidth() * 0.5 - 125.0;
             
            return true;
        });
        
        fullscreenScene.resume();
    } 

    if(fullscreen)
    {
        sceneFX.pause();
        
        var fullscreenIcon = getElement("fullscrn_" + sceneID);
        fullscreenIcon.style.opacity = fullscreenIcon.style.zIndex = 1.0;            
    }
    else sceneFX.resume();
    
    updateSceneState(sceneID);
}

function clearScene(sceneID)
{
    var sceneFX = scenes[sceneID];
    
    if(fullscreenSceneID != sceneID)
    {
        resetAllScenes();
        sceneFX.resume();
        sceneFX.clear();
    
        updateSceneState(sceneID);
        
    } else {
        sceneFX = fullscreenScene;
        sceneFX.clear();
    } 
    
    sceneFX.emitters.every( function(emitter)
    {          
        emitter.pos.x = sceneFX.getWidth() * 0.5 / sceneFX.getScaleFactor();
        emitter.pos.y = sceneFX.getHeight() * 0.5 / sceneFX.getScaleFactor();
        emitter.vel.rotate(random(0.0,360.0));
         
        return true;
    });  
}

function resetScene(sceneID)
{
    var sceneFX = scenes[sceneID];
    clearFullscreenScene(sceneID);
    
    if(sceneFX.running)
    {
        sceneFX.pause();
        updateSceneState(sceneID);
    }
}

function resetAllScenes() {
    for(var i=0; i<scenes.length; ++i) resetScene(i);
}

function editFX(fxTitle)
{
    
}

function downloadFX(sceneID,fxName)
{
    var sceneFX = scenes[sceneID];
    var selectedFX = sceneFX.emitters[0];
    
    var encodedFX = FX_fileHeader()+"__FX_DATA_CACHE[\"__cache_import_fx\"] = ";
    encodedFX += selectedFX.encodeJSON() + ";";
        
    downloadData(fxName+".fx",encodedFX);
}

function downloadData(filename,data)
{
    var a = document.createElement("a"),
    file = new Blob([data]);
        
    if(!window.navigator.msSaveOrOpenBlob)
    {            
        a.href = URL.createObjectURL(file);
        a.download = filename;
                    
        document.body.appendChild(a);
        a.click(); 
                    
        setTimeout(function()
        {
            window.URL.revokeObjectURL(a.href);
            document.body.removeChild(a);  
        }, 0);
         
    } else window.navigator.msSaveOrOpenBlob(file,filename);
}
                 
function createPlayerFX(fxPath,zoomExp,fxTitle,fxDescription)
{
    var playerID = __playerID++;

    var fxThumbnailPath = "Player/Res/Thumbnails/" + fxTitle + ".thumb";
    fxThumbnailPath = fxThumbnailPath.replace(/ /g,"%20");
    
    var playerConteiner = document.createElement('div');
    playerConteiner.className = "playerContainer";
    
    playerConteiner.innerHTML = "<div id='" + playerID + "' class='previewContainer' onmouseover='__playerOnFocusID = this.id;' onmouseout='__playerOnFocusID = -1;'>\n" +
                                "    <canvas id='canvas_" + playerID + "' class='preview unselectable'></canvas>\n" +
                                "    <img id='thumb_" + playerID + "' class='thumbnail unselectable' src=" + fxThumbnailPath + ">\n" +
                                "    <div id='fullscrn_" + playerID + "' class='exitFullscrnContainer'>\n" +
                                "        <img id='" + playerID + "' class='exitFullscrnBtn playerBtn' src='Player/Res/exit_fullscreen.png' onclick='runFullscreenScene(this.id,false);'>\n" +
                                "    </div>\n" +
                                "   <img id='play_" + playerID + "' class='playPauseBtn playerBtn' src='Player/Res/play.png' onclick='playPauseScene(this.id.slice(5));'>\n" +
                                "   <img id='" + playerID + "' class='fullscrnBtn playerBtn' src='Player/Res/fullscreen.png' onclick='runFullscreenScene(this.id,true);'>\n" +
                                "   <img id='" + playerID + "' class='resetBtn playerBtn' src='Player/Res/reset.png' onclick='clearScene(this.id);'>\n" +
                                "</div>\n" +
                                "<div class='dcrContainer'>\n" +
                                "    <h2 class='dcrTitle'>" + fxTitle + "</h2>\n" +
                                "    <div class='dcrLabel'>" + fxDescription + "</div>\n" +
                                "    <input type=\"button\" class=\"editBtn\" onclick=\"location.href='https://www.blackravenproduction.com/portfolio/lorenzo_vannuccini/PyroBit_FX_Studio/?sampleID=" + fxTitle + "';\" value=\"Open in Editor FX\" />" +
                                "    <button id='" + playerID + "' class='downloadBtn' onclick='downloadFX(this.id,\"" + fxTitle + "\");'>Download FX</button>\n" +
                                "</div>\n";
    
    var playersContainer = getElement("playersFX");              
    playersContainer.appendChild(playerConteiner);
    
    var sceneFX = scenes[playerID] = new SceneFX();
    
    sceneFX.bindCanvas(getElement("canvas_" + playerID));
    
    sceneFX.pause();
    sceneFX.playerID = playerID;
    sceneFX.setScaleFactor(zoomExp);
    
    var emitter = sceneFX.createEmitter(fxPath, function(thisEmitter)
    {
        var scene = thisEmitter.context;
        
        thisEmitter.pos.x = scene.getWidth()*0.5 / scene.getScaleFactor();
        thisEmitter.pos.y = scene.getHeight()*0.5  / scene.getScaleFactor();
    });
    
    sceneFX.setSimulation( function(thisScene,dt)
    {
        var cursorSceneCoords = thisScene.screenToClient(cursorAbsoluteX,cursorAbsoluteY);
        var centerSceneCoords = new vector2d(thisScene.getWidth()*0.5 / thisScene.getScaleFactor(), thisScene.getHeight()*0.5  / thisScene.getScaleFactor());
        
        var targetCoords = ((thisScene.playerID == null || __playerOnFocusID == thisScene.playerID) ? cursorSceneCoords : centerSceneCoords);
        thisScene.setProgramData("target", targetCoords);
    });                        
}
