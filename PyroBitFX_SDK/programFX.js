//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var programFX = function(programType,script,errorHandler)
{   
    this.data = {};
    this.instanceData = {};
    
    this.clear();
    this.clearData();
    this.clearObservers();
    this.type = programType;
    this.setErrorHandler(errorHandler);
    if(script != null) this.compile(script);
}

programFX.prototype.clear = function()
{
    this.clearData();
    this.program = null;
    this.source = "";
}

programFX.prototype.setErrorHandler = function(errorHandler)
{
    var thisProgram = this;
    this.errorHandler = function(err)
    {
        thisProgram.program = null;
        if(errorHandler != null) errorHandler(((err != null) ? err : "unknown error"));    
    };
}

programFX.prototype.setObserver = function(eventID,eventHandler) {
    if(eventHandler != null) this.observers[eventID] = eventHandler;
    else delete this.observers[eventID];
}

programFX.prototype.hasObserver = function(eventID) {
    return (this.observers[eventID] != null);
}

programFX.prototype.clearObservers = function() {
    this.observers = {};   
}

programFX.prototype.clearData = function()
{
    for(var key in this.data) delete this.data[key];
    for(var key in this.instanceData) delete this.instanceData[key];
}

programFX.prototype.isValid = function() {
    return (this.program != null);
}

programFX.prototype.compile = function(script)
{   
    this.clear();
    if(script == null || script.length < 1) return false;
   
    this.reserved = {};
    this.source = script;
     
    var sandbox = createSandboxFXSL(this);
    __sandboxFXSL__compileMode = true;
     
    var compileStatus = sandbox.run(sandbox.script,this.errorHandler);
    if(compileStatus) this.program = sandbox.program;
    sandbox.clear();
    
    if(compileStatus) this.run(null);
    __sandboxFXSL__compileMode = false;
    this.clearData();
            
    return (compileStatus && this.program != null);    
}

programFX.prototype.run = function(fx,dt)
{       
    if(this.program == null) return false;
    if(dt == null) dt = 0.0;
              
    try 
    {             
        if(this.type == "EMITTER_PROGRAM")
        {   
            if(fx == null) fx = new EmitterFX();
                     
            this.program( this.instanceData,
                          fx.id,
                          dt,
                          fx.timeElapsed,
                          (fx.context ? new vector2d(fx.context.getWidth() / fx.context.getScaleFactor(),fx.context.getHeight() / fx.context.getScaleFactor()) : new vector2d(0.0)), 
                          new vector2d(fx.pos.x,fx.pos.y),
                          new vector2d(fx.vel.x,fx.vel.y),
                          null,
                          new vector2d(fx.emitterPatternTrimX,fx.emitterPatternTrimY),
                          fx.emitterPatternSize,
                          fx.emitterPatternBias,
                          new vector2d(fx.flarePatternTrimX,fx.flarePatternTrimY),
                          fx.flarePatternSize,
                          fx.flarePatternBias );
        }
        else if(this.type == "SOURCE_PROGRAM")
        {   
            if(fx == null) fx = new SourceFX();
                                
            this.program( this.instanceData,
                          fx.id,
                          dt,
                          fx.timeElapsed,
                          (fx.context ? new vector2d(fx.context.getWidth() / fx.context.getScaleFactor(),fx.context.getHeight() / fx.context.getScaleFactor()) : new vector2d(0.0)),  
                          ((fx.life == Infinity) ? -1.0 : fx.life),
                          fx.slope,
                          fx.spanMin,
                          fx.spanMax,
                          fx.spawningAmountMin,
                          fx.spawningAmountMax,
                          fx.spawningTimeMin,
                          fx.spawningTimeMax,
                          fx.particlesLifeMin,
                          fx.particlesLifeMax,
                          fx.particlesDecayExp,
                          fx.particlesTransitionStartExp,
                          fx.particlesTransitionFadeExp,
                          fx.particlesSpeedMin,
                          fx.particlesSpeedMax,
                          fx.particlesVelocityBiasMin,
                          fx.particlesVelocityBiasMax,
                          new vector2d(fx.particlesAcceleration.x,fx.particlesAcceleration.y),
                          fx.particlesTorqueMin,
                          fx.particlesTorqueMax,
                          fx.particlesRotationMin,
                          fx.particlesRotationMax,
                          fx.particlesSizeMin,
                          fx.particlesSizeMax,
                          fx.particlesSizeVariationMin,
                          fx.particlesSizeVariationMax,
                          fx.particlesTorqueFrictionExp,
                          fx.particlesFrictionExp,
                          fx.particlesInertiaExp );
                                         
                     
        }
        else if(this.type == "PARTICLE_PROGRAM")
        {  
            if(fx == null) fx = new ParticleFX();
                      
            this.program( this.instanceData,
                          fx.id,
                          dt,
                          fx.timeElapsed,
                          (fx.context ? new vector2d(fx.context.getWidth() / fx.context.getScaleFactor(),fx.context.getHeight() / fx.context.getScaleFactor()) : new vector2d(0.0)), 
                          new vector2d(fx.pos.x,fx.pos.y),
                          new vector2d(fx.vel.x,fx.vel.y),
                          fx.torque,
                          fx.spin,
                          fx.size,
                          fx.life );
        }                                           
    }
    catch(err) {
        this.errorHandler(err);
        return false;
    }

    if(this.type == "EMITTER_PROGRAM")
    {   
        if(this.reserved.fx_Position != null) {
            fx.pos.x = this.reserved.fx_Position.x;
            fx.pos.y = this.reserved.fx_Position.y;
        }

        if(this.reserved.fx_Velocity != null) {
            fx.vel.x = this.reserved.fx_Velocity.x;
            fx.vel.y = this.reserved.fx_Velocity.y;
        }
        
        fx.fixedSlope = this.reserved.fx_Slope;
        
        if(this.reserved.fx_BasePatternTrim != null) {
            fx.emitterPatternTrimX = this.reserved.fx_BasePatternTrim.x;
            fx.emitterPatternTrimY = this.reserved.fx_BasePatternTrim.y;
        }
        
        if(this.reserved.fx_FlarePatternTrim != null) {
            fx.flarePatternTrimX = this.reserved.fx_FlarePatternTrim.x;
            fx.flarePatternTrimY = this.reserved.fx_FlarePatternTrim.y;
        }
        
        if(this.reserved.fx_BasePatternSize != null) fx.emitterPatternSize = this.reserved.fx_BasePatternSize;
        if(this.reserved.fx_BasePatternBias != null) fx.emitterPatternBias = this.reserved.fx_BasePatternBias;
        if(this.reserved.fx_FlarePatternSize != null) fx.flarePatternSize = this.reserved.fx_FlarePatternSize;
        if(this.reserved.fx_FlarePatternBias != null) fx.flarePatternBias = this.reserved.fx_FlarePatternBias;
    }
    else if(this.type == "SOURCE_PROGRAM")
    {
        if(this.reserved.fx_Life != null) fx.life = ((this.reserved.fx_Life >= 0.0) ? this.reserved.fx_Life : Infinity);
        if(this.reserved.fx_Slope != null) fx.slope = this.reserved.fx_Slope;
        
        if(this.reserved.fx_EmitSpanMin != null) {
            fx.spanMin.x = this.reserved.fx_EmitSpanMin.x;
            fx.spanMin.y = this.reserved.fx_EmitSpanMin.y;
        }
        
        if(this.reserved.fx_EmitSpanMax != null) {
            fx.spanMax.x = this.reserved.fx_EmitSpanMax.x;
            fx.spanMax.y = this.reserved.fx_EmitSpanMax.y;
        }
        
        if(this.reserved.fx_EmitAmountMin != null) fx.spawningAmountMin = this.reserved.fx_EmitAmountMin;
        if(this.reserved.fx_EmitAmountMax != null) fx.spawningAmountMax = this.reserved.fx_EmitAmountMax;
        if(this.reserved.fx_EmitFrequencyMin != null) fx.spawningTimeMin = this.reserved.fx_EmitFrequencyMin;
        if(this.reserved.fx_EmitFrequencyMax != null) fx.spawningTimeMax = this.reserved.fx_EmitFrequencyMax;                        
        if(this.reserved.fx_ParticlesLifeMin != null) fx.particlesLifeMin = this.reserved.fx_ParticlesLifeMin;
        if(this.reserved.fx_ParticlesLifeMax != null) fx.particlesLifeMax = this.reserved.fx_ParticlesLifeMax;
        if(this.reserved.fx_ParticlesDecayFadeExp != null) fx.particlesDecayExp = this.reserved.fx_ParticlesDecayFadeExp;
        if(this.reserved.fx_ParticlesTransitionPivot != null) fx.particlesTransitionStartExp = this.reserved.fx_ParticlesTransitionPivot;
        if(this.reserved.fx_ParticlesTransitionFadeExp != null) fx.particlesTransitionFadeExp = this.reserved.fx_ParticlesTransitionFadeExp;
        if(this.reserved.fx_ParticlesInitialSpeedMin != null) fx.particlesSpeedMin = this.reserved.fx_ParticlesInitialSpeedMin;
        if(this.reserved.fx_ParticlesInitialSpeedMax != null) fx.particlesSpeedMax = this.reserved.fx_ParticlesInitialSpeedMax;
        if(this.reserved.fx_ParticlesVelocityBiasMin != null) fx.particlesVelocityBiasMin = this.reserved.fx_ParticlesVelocityBiasMin;
        if(this.reserved.fx_ParticlesVelocityBiasMax != null) fx.particlesVelocityBiasMax = this.reserved.fx_ParticlesVelocityBiasMax;
        
        if(this.reserved.fx_ParticlesConstAcceleration != null) {
            fx.particlesAcceleration.x = this.reserved.fx_ParticlesConstAcceleration.x;
            fx.particlesAcceleration.y = this.reserved.fx_ParticlesConstAcceleration.y;
        } 
        
        if(this.reserved.fx_ParticlesTorqueForceMin != null) fx.particlesTorqueMin = this.reserved.fx_ParticlesTorqueForceMin;
        if(this.reserved.fx_ParticlesTorqueForceMax != null) fx.particlesTorqueMax = this.reserved.fx_ParticlesTorqueForceMax;
        if(this.reserved.fx_ParticlesInitialRotationMin != null) fx.particlesRotationMin = this.reserved.fx_ParticlesInitialRotationMin;
        if(this.reserved.fx_ParticlesInitialRotationMax != null) fx.particlesRotationMax = this.reserved.fx_ParticlesInitialRotationMax;
        if(this.reserved.fx_ParticlesInitialSizeMin != null) fx.particlesSizeMin = this.reserved.fx_ParticlesInitialSizeMin;
        if(this.reserved.fx_ParticlesInitialSizeMax != null) fx.particlesSizeMax = this.reserved.fx_ParticlesInitialSizeMax;
        if(this.reserved.fx_ParticlesSizeVariationMin != null) fx.particlesSizeVariationMin = this.reserved.fx_ParticlesSizeVariationMin;
        if(this.reserved.fx_ParticlesSizeVariationMax != null) fx.particlesSizeVariationMax = this.reserved.fx_ParticlesSizeVariationMax;
        if(this.reserved.fx_ParticlesFrictionTorqueExp != null) fx.particlesTorqueFrictionExp = this.reserved.fx_ParticlesFrictionTorqueExp;
        if(this.reserved.fx_ParticlesFrictionExp != null) fx.particlesFrictionExp = this.reserved.fx_ParticlesFrictionExp;
        if(this.reserved.fx_ParticlesInertiaExp != null) fx.particlesInertiaExp = this.reserved.fx_ParticlesInertiaExp;
    }     
    else if(this.type == "PARTICLE_PROGRAM")
    {
        if(this.reserved.fx_Position != null) {
            fx.pos.x = this.reserved.fx_Position.x;
            fx.pos.y = this.reserved.fx_Position.y;
        }
        
        if(this.reserved.fx_Velocity != null) {
            fx.vel.x = this.reserved.fx_Velocity.x;
            fx.vel.y = this.reserved.fx_Velocity.y;
        }
        
        if(this.reserved.fx_Torque != null) fx.torque = this.reserved.fx_Torque;
        if(this.reserved.fx_Slope != null) fx.spin = this.reserved.fx_Slope;
        if(this.reserved.fx_Size != null) fx.size = this.reserved.fx_Size;
        if(this.reserved.fx_Life != null) fx.life = this.reserved.fx_Life;
    }
    
    return true;       
}
