//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var __sandboxFXSL__compileMode = false;
var __sandboxFXSL__enableLogging = true;
 
function createSandboxFXSL(context)
{                       
    var sandbox = new Sandbox();
    sandbox.program = null;
    sandbox.script = "";
    sandbox.APIs = {};

    var stdio = []; // standard input / otputs (program-type based)
    
    stdio.push("instance");
    stdio.push("fx_ID");
    stdio.push("fx_Dt");
    stdio.push("fx_TimeElapsed");
    stdio.push("fx_Viewport");
       
    if(context.type == "EMITTER_PROGRAM")
    {
         stdio.push("fx_Position");
         stdio.push("fx_Velocity");
         stdio.push("fx_Slope");
         stdio.push("fx_BasePatternTrim");
         stdio.push("fx_BasePatternSize");
         stdio.push("fx_BasePatternBias");
         stdio.push("fx_FlarePatternTrim");
         stdio.push("fx_FlarePatternSize");
         stdio.push("fx_FlarePatternBias");
    }
    else if(context.type == "SOURCE_PROGRAM")
    {
         stdio.push("fx_Life");
         stdio.push("fx_Slope");
         stdio.push("fx_EmitSpanMin");
         stdio.push("fx_EmitSpanMax");
         stdio.push("fx_EmitAmountMin");
         stdio.push("fx_EmitAmountMax");
         stdio.push("fx_EmitFrequencyMin");
         stdio.push("fx_EmitFrequencyMax");
         stdio.push("fx_ParticlesLifeMin");
         stdio.push("fx_ParticlesLifeMax");
         stdio.push("fx_ParticlesDecayFadeExp");
         stdio.push("fx_ParticlesTransitionPivot");
         stdio.push("fx_ParticlesTransitionFadeExp");
         stdio.push("fx_ParticlesInitialSpeedMin");
         stdio.push("fx_ParticlesInitialSpeedMax");
         stdio.push("fx_ParticlesVelocityBiasMin");
         stdio.push("fx_ParticlesVelocityBiasMax");
         stdio.push("fx_ParticlesConstAcceleration");
         stdio.push("fx_ParticlesTorqueForceMin");
         stdio.push("fx_ParticlesTorqueForceMax");
         stdio.push("fx_ParticlesInitialRotationMin");
         stdio.push("fx_ParticlesInitialRotationMax");
         stdio.push("fx_ParticlesInitialSizeMin");
         stdio.push("fx_ParticlesInitialSizeMax");
         stdio.push("fx_ParticlesSizeVariationMin");
         stdio.push("fx_ParticlesSizeVariationMax");
         stdio.push("fx_ParticlesFrictionTorqueExp");
         stdio.push("fx_ParticlesFrictionExp");
         stdio.push("fx_ParticlesInertiaExp");
    }     
    else if(context.type == "PARTICLE_PROGRAM")
    {
         stdio.push("fx_Position");
         stdio.push("fx_Velocity");
         stdio.push("fx_Torque");
         stdio.push("fx_Slope");
         stdio.push("fx_Size");
         stdio.push("fx_Life");
    }
    
    // constants / native APIs
    sandbox.APIs["PI"] = Math.PI;
    sandbox.APIs["PIover180"] = PIover180;
    sandbox.APIs["PIover360"] = PIover360;
    
    sandbox.APIs["vec2"] = function(x,y) { if(x == null) x = 0.0; return ((x.x != null) ? new vector2d(x.x,x.y) : new vector2d(x,y)); };
    
    sandbox.APIs["dot"] = function(a,b) { return a.dot(b); };
    sandbox.APIs["cross"] = function(a,b) { return a.cross(b); };
    sandbox.APIs["slope"] = function(a,b) { var na = new vector2d(a.x,a.y); var nb = new vector2d(b.x,b.y); na.normalize(); nb.normalize(); return na.slope(nb); };
    sandbox.APIs["normalize"] = function(v) { return new vector2d(v.x,v.y).normalize(); };
    
    sandbox.APIs["length"] = function(v) { return v.length(); };
    sandbox.APIs["squaredLength"] = function(v) { return v.squaredLength(); };
    sandbox.APIs["distance"] = function(a,b) { return (new vector2d(a.x-b.x,a.y-b.y)).length(); };
    sandbox.APIs["squaredDistance"] = function(a,b) { return (new vector2d(a.x-b.x,a.y-b.y)).squaredLength(); };
    
    sandbox.APIs["rad"] = function(deg) { return deg * PIover180; };
    sandbox.APIs["deg"] = function(rad) { return rad * 180.0 / Math.PI; };

    sandbox.APIs["sin"] = function(v) { return Math.sin(v); };
    sandbox.APIs["sin2D"] = function(v) { return new vector2d(Math.sin(v.x),Math.sin(v.y)); };
    sandbox.APIs["asin"] = function(v) { return Math.asin(clamp(v,0.0,1.0)); };
    sandbox.APIs["asin2D"] = function(v) { return new vector2d(Math.asin(clamp(v.x,0.0,1.0)),Math.asin(clamp(v.y,0.0,1.0))); };
    sandbox.APIs["cos"] = function(v) { return Math.cos(v); };
    sandbox.APIs["cos2D"] = function(v) { return new vector2d(Math.cos(v.x),Math.cos(v.y)); };
    sandbox.APIs["acos"] = function(v) { return Math.acos(clamp(v,0.0,1.0)); };
    sandbox.APIs["acos2D"] = function(v) { return new vector2d(Math.acos(clamp(v.x,0.0,1.0)),Math.acos(clamp(v.y,0.0,1.0))); };
    sandbox.APIs["tan"] = function(v) { return Math.tan(v); };
    sandbox.APIs["tan2D"] = function(v) { return new vector2d(Math.tan(v.x),Math.tan(v.y)); };
    sandbox.APIs["atan"] = function(v) { return Math.atan(v); };
    sandbox.APIs["atan2D"] = function(v) { return new vector2d(Math.atan(v.x),Math.atan(v.y)); };
    
    sandbox.APIs["pow"] = function(b,e) { return Math.pow(b,((e != null) ? e : 2.0)); };
    sandbox.APIs["pow2D"] = function(v,e) { return new vector2d(Math.pow(v.x,((e != null) ? e.x : 2.0)),Math.pow(v.y,((e != null) ? e.y : 2.0))); };
    sandbox.APIs["sqrt"] = function(x) { return Math.sqrt(x); };
    sandbox.APIs["sqrt2D"] = function(v) { return new vector2d(Math.sqrt(v.x),Math.sqrt(v.y)); };
    sandbox.APIs["floor"] = function(x) { return Math.floor(x); };
    sandbox.APIs["floor2D"] = function(v) { return new vector2d(Math.floor(v.x),Math.floor(v.y)); };
    sandbox.APIs["ceil"] = function(x) { return Math.ceil(x); };
    sandbox.APIs["ceil2D"] = function(v) { return new vector2d(Math.ceil(v.x),Math.ceil(v.y)); };
    sandbox.APIs["round"] = function(x) { return Math.round(x); };
    sandbox.APIs["round2D"] = function(v) { return new vector2d(Math.round(v.x),Math.round(v.y)); };
    
    sandbox.APIs["abs"] = function(v) { return abs(v); };
    sandbox.APIs["abs2D"] = function(v) { return new vector2d(abs(v.x),abs(v.y)); };
    sandbox.APIs["max"] = function(a,b) { return max(a,b); };
    sandbox.APIs["max2D"] = function(a,b) { return max(max(a.x,a.y),max(b.x,b.y)); };
    sandbox.APIs["min"] = function(a,b) { return min(a,b); };
    sandbox.APIs["min2D"] = function(a,b) { return min(min(a.x,a.y),min(b.x,b.y)); };
    sandbox.APIs["clamp"] = function(x,min,max) { return clamp(x,min,max); };
    sandbox.APIs["clamp2D"] = function(v,min,max) { return new vector2d(clamp(v.x,min.x,max.x),clamp(v.y,min.y,max.y)); };
    
    sandbox.APIs["mix"] = function(s,e,t) { return mix(s,e,t); };
    sandbox.APIs["mix2D"] = function(s,e,t) { return new vector2d(mix(s.x,e.x,t.x),mix(s.y,e.y,t.y)); };
    sandbox.APIs["smoothstep"] = function(e1,e2,p) { return smoothstep(e1,e2,p); };
    sandbox.APIs["smoothstep2D"] = function(e1,e2,p) { return new vector2d(smoothstep(e1.x,e2.x,p.x),smoothstep(e1.y,e2.y,p.y)); };
    
    sandbox.APIs["random"] = function(min,max) { return random(min,max); };
    sandbox.APIs["random2D"] = function(min,max) { return new vector2d(random(min,max),random(min,max)); };
        
    sandbox.APIs["log"] = function(obj) { if(__sandboxFXSL__compileMode||!__sandboxFXSL__enableLogging) return; /*var dcr = JSON.stringify(obj,null,4); alert(dcr);*/ console.log(obj); };
    sandbox.APIs["triggerEvent"] = function(eventID) { if(__sandboxFXSL__compileMode) return; if(context.observers[eventID] != null) context.observers[eventID](); };
    
    sandbox.APIs["data"] = context.data;
    sandbox.APIs["reserved"] = context.reserved;
    
    for(var name in sandbox.APIs) sandbox.script += "var " + name + " = this.APIs." + name + ";\n";
    sandbox.script += "\nthis.program = function(" + stdio + "){ \"use strict\";\nfunction __main()\n{";
    sandbox.script += "\"use strict\"; \n\n" + context.source + "\n\n};\n__main();\n";
    
    stdio.every(function(name) {
        sandbox.script += "reserved." + name + " = " + name + ";\n";
        return true;
    });
    
    sandbox.script += "};\n";
    return sandbox;
}
