//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var EmitterFX = function(context,fxID,initializationHandler)
{
    this.id = 0;
    this.context = context;
    this.setSimulation(null);
    this.enableFrameCollisions = true;
    this.frameLeft = this.frameTop = this.frameRight = this.frameBottom = 0.0;

    this.pos = new vector2d(0.0,0.0);
    this.oldPos = new vector2d(this.pos.x,this.pos.y);
    
    this.slope = 0.0;
    this.fixedSlope = null;   
    this.vel = new vector2d(1.0,0.0);
    this.particlesForcesAccumulator = new vector2d(0.0);
    
    this.emitterPatternTrimX = 0.0;
    this.emitterPatternTrimY = 0.0;
    this.emitterPatternBias = 0.0;
    this.emitterPatternSize = 200.0;
    
    this.flarePatternTrimX = 0.0;
    this.flarePatternTrimY = 0.0;
    this.flarePatternBias = 0.0;
    this.flarePatternSize = 500.0;
    this.flarePatternFlickeringExp = 0.5;
    
    this.sources = [];                                   
    this.timeElapsed = 0.0;
    this.activeProgramFX = true; 
    this.programData = {}; // shared in Emitter's context
    this.program = null;
    this.observers = {};
    
    this.flarePatternImage = null;
    this.emitterPatternImage = null;
    this.flushParticlesTimeLapsed = 0.0;
    
    if(fxID != null) this.load(fxID,initializationHandler);
}

EmitterFX.prototype.setProgram = function(source)
{    
    if(source == null) {
        this.program = null;
        return true;
    }
    
    this.program = new programFX("EMITTER_PROGRAM");
    this.program.setErrorHandler( function(err){ alert(err); });
    this.program.data = this.programData; // shared data between same context programs
    
    return this.program.compile(source);
}

EmitterFX.prototype.setObserver = function(eventID,eventHandler) {
    this.observers[eventID] = eventHandler;
}

EmitterFX.prototype.hasObserver = function(eventID) {
    return (this.observers[eventID] != null);
}

EmitterFX.prototype.clearObservers = function() {
    this.observers = {};   
}

EmitterFX.prototype.refreshObservers = function()
{
    if(this.program != null) this.program.clearObservers();
    
    this.sources.every( function(source)
    {
        if(source.particleProgram != null) source.particleProgram.clearObservers();
        if(source.program != null) source.program.clearObservers();
        
        return true;
    });
    
    var thisEmitter = this;
    for(var eventID in this.observers)
    {
        var eventHandler = thisEmitter.observers[eventID]; 
        
        var emitterEventHandler = null;
        var sourceEventHandler = null;
        var particleEventHandler = null;

        if(eventHandler != null)
        {
            emitterEventHandler = function() {
                eventHandler(thisEmitter,"EMITTER_PROGRAM",eventID);
            };
            
            sourceEventHandler = function() {
                eventHandler(thisEmitter,"SOURCE_PROGRAM",eventID);
            };
            
            particleEventHandler = function() {
                eventHandler(thisEmitter,"PARTICLE_PROGRAM",eventID);
            };
        }

        if(thisEmitter.program != null) thisEmitter.program.setObserver(eventID, emitterEventHandler);
    
        thisEmitter.sources.every( function(source)
        {
            if(source.program != null) source.program.setObserver(eventID, sourceEventHandler);
            if(source.particleProgram != null) source.particleProgram.setObserver(eventID, particleEventHandler);
            
            return true;
        });    
    }
}

EmitterFX.prototype.clearData = function() {
    this.program.clearData();
}

EmitterFX.prototype.setProgramData = function(key,value)
{
    if(value != null) this.programData[key] = value;
    else delete this.programData[key];
}

EmitterFX.prototype.clearProgramData = function() {
    if(this.program != null) this.program.clearData();
}

EmitterFX.prototype.setSimulation = function(functor) {
    this.updatingFunctor = functor;
}

EmitterFX.prototype.encodeJSON = function() {
    return JSON.stringify(this.copy());  
}

EmitterFX.prototype.load = function(fxID,initializationHandler)
{     
    this.free();
    
    var thisEmitter = this;
    preloadFX(fxID, function()
    {
        var jsonDecodedObj = __FX_DATA_CACHE[fxID];
         
        if(jsonDecodedObj.pos != null) {
            thisEmitter.pos.x = jsonDecodedObj.pos.x;
            thisEmitter.pos.y = jsonDecodedObj.pos.y;
        }
            
        thisEmitter.slope = 0.0;
        thisEmitter.fixedSlope = null; 
        thisEmitter.vel.x = thisEmitter.vel.y = 0.0;
        thisEmitter.particlesForcesAccumulator = new vector2d(0.0);
        thisEmitter.frameLeft = thisEmitter.frameTop = thisEmitter.frameRight = thisEmitter.frameBottom = 0.0;
    
        if(jsonDecodedObj.emitterPatternImage != null) thisEmitter.setEmitterPattern(jsonDecodedObj.emitterPatternImage.path);
        thisEmitter.emitterPatternTrimX = jsonDecodedObj.emitterPatternTrimX;
        thisEmitter.emitterPatternTrimY = jsonDecodedObj.emitterPatternTrimY;
        thisEmitter.emitterPatternBias = jsonDecodedObj.emitterPatternBias;
        thisEmitter.emitterPatternSize = jsonDecodedObj.emitterPatternSize;
                
        if(jsonDecodedObj.flarePatternImage != null) thisEmitter.setFlarePattern(jsonDecodedObj.flarePatternImage.path);
        thisEmitter.flarePatternTrimX = jsonDecodedObj.flarePatternTrimX;
        thisEmitter.flarePatternTrimY = jsonDecodedObj.flarePatternTrimY;
        thisEmitter.flarePatternBias = jsonDecodedObj.flarePatternBias;
        thisEmitter.flarePatternSize = jsonDecodedObj.flarePatternSize;
        thisEmitter.flarePatternFlickeringExp = jsonDecodedObj.flarePatternFlickeringExp;  
        if(jsonDecodedObj.program != null) thisEmitter.setProgram(jsonDecodedObj.program.source);
        if(jsonDecodedObj.activeProgramFX != null) thisEmitter.activeProgramFX = jsonDecodedObj.activeProgramFX; 
        thisEmitter.importSources(jsonDecodedObj.sources);
        
        if(initializationHandler != null) initializationHandler(thisEmitter); 
    });
}

EmitterFX.prototype.copy = function(context)
{
    var newEmitterFX = new EmitterFX(context);
    
    newEmitterFX.pos.x = this.pos.x;
    newEmitterFX.pos.y = this.pos.y;
    
    if(this.emitterPatternImage != null) newEmitterFX.setEmitterPattern(this.emitterPatternImage.path);   
    newEmitterFX.emitterPatternTrimX = this.emitterPatternTrimX;
    newEmitterFX.emitterPatternTrimY = this.emitterPatternTrimY;
    newEmitterFX.emitterPatternBias = this.emitterPatternBias;
    newEmitterFX.emitterPatternSize = this.emitterPatternSize;
    
    if(this.flarePatternImage != null) newEmitterFX.setFlarePattern(this.flarePatternImage.path);   
    newEmitterFX.flarePatternTrimX = this.flarePatternTrimX;
    newEmitterFX.flarePatternTrimY = this.flarePatternTrimY;
    newEmitterFX.flarePatternBias = this.flarePatternBias;
    newEmitterFX.flarePatternSize = this.flarePatternSize;
    newEmitterFX.flarePatternFlickeringExp = this.flarePatternFlickeringExp;  

    if(this.program != null) newEmitterFX.setProgram(this.program.source);
    if(this.activeProgramFX != null) newEmitterFX.activeProgramFX = this.activeProgramFX;
    
    newEmitterFX.importSources(this.sources);
    return newEmitterFX;
}

EmitterFX.prototype.importSources = function(otherSources)
{      
    var thisEmitterFX = this;
    otherSources.every(function(source)
    {              
        var newSourceFX = thisEmitterFX.createSource();
        
        if(source.label != null) newSourceFX.label = source.label;
        if(source.delay != null) newSourceFX.delay = source.delay;
        if(source.loop != null) newSourceFX.loop = source.loop;
        if(source.life != null) newSourceFX.life = source.life;
        if(newSourceFX.life == null) newSourceFX.life = Infinity;
        
        if(source.spanMin != null) {
            newSourceFX.spanMin.x = source.spanMin.x;
            newSourceFX.spanMin.y = source.spanMin.y;
        } 
        
        if(source.spanMax != null) {
            newSourceFX.spanMax.x = source.spanMax.x;
            newSourceFX.spanMax.y = source.spanMax.y;
        } 
        
        if(source.spawningTimeMin != null) newSourceFX.spawningTimeMin = source.spawningTimeMin;
        if(source.spawningTimeMax != null) newSourceFX.spawningTimeMax = source.spawningTimeMax;
        if(source.spawningAmountMin != null) newSourceFX.spawningAmountMin = source.spawningAmountMin;
        if(source.spawningAmountMax != null) newSourceFX.spawningAmountMax = source.spawningAmountMax;

        if(source.particlesLifeMin != null) newSourceFX.particlesLifeMin = source.particlesLifeMin;
        if(source.particlesLifeMax != null) newSourceFX.particlesLifeMax = source.particlesLifeMax;
        
        if(source.particlesDecayExp != null) newSourceFX.particlesDecayExp = source.particlesDecayExp;
        if(source.particlesTransitionStartExp != null) newSourceFX.particlesTransitionStartExp = source.particlesTransitionStartExp;
        if(source.particlesTransitionFadeExp != null) newSourceFX.particlesTransitionFadeExp = source.particlesTransitionFadeExp;
                        
        if(source.particlesSpeedMin != null) newSourceFX.particlesSpeedMin = source.particlesSpeedMin;
        if(source.particlesSpeedMax != null) newSourceFX.particlesSpeedMax = source.particlesSpeedMax;
        
        if(source.particlesVelocityBiasMin != null) newSourceFX.particlesVelocityBiasMin = source.particlesVelocityBiasMin;
        if(source.particlesVelocityBiasMax != null) newSourceFX.particlesVelocityBiasMax = source.particlesVelocityBiasMax;
        
        if(source.particlesTorqueMin != null) newSourceFX.particlesTorqueMin = source.particlesTorqueMin;
        if(source.particlesTorqueMax != null) newSourceFX.particlesTorqueMax = source.particlesTorqueMax;
        
        if(source.particlesRotationMin != null) newSourceFX.particlesRotationMin = source.particlesRotationMin;
        if(source.particlesRotationMax != null) newSourceFX.particlesRotationMax = source.particlesRotationMax;
        
        if(source.particlesSizeMin != null) newSourceFX.particlesSizeMin = source.particlesSizeMin;
        if(source.particlesSizeMax != null) newSourceFX.particlesSizeMax = source.particlesSizeMax;
        
        if(source.particlesSizeVariationMin != null) newSourceFX.particlesSizeVariationMin = source.particlesSizeVariationMin;
        if(source.particlesSizeVariationMax != null) newSourceFX.particlesSizeVariationMax = source.particlesSizeVariationMax;
    
        if(source.particlesInertiaExp != null) newSourceFX.particlesInertiaExp = source.particlesInertiaExp;
        if(source.particlesFrictionExp != null) newSourceFX.particlesFrictionExp = source.particlesFrictionExp;
        if(source.particlesTorqueFrictionExp != null) newSourceFX.particlesTorqueFrictionExp = source.particlesTorqueFrictionExp;
        
        if(source.particlesAcceleration != null) {
            newSourceFX.particlesAcceleration.x = source.particlesAcceleration.x;
            newSourceFX.particlesAcceleration.y = source.particlesAcceleration.y;  
        }
        
        if(source.program != null) newSourceFX.setProgram(source.program.source);
        if(source.activeProgramFX != null) newSourceFX.activeProgramFX = source.activeProgramFX;
        if(source.particleProgram != null) newSourceFX.setParticleProgram(source.particleProgram.source);
        if(source.activeParticleProgramFX != null) newSourceFX.activeParticleProgramFX = source.activeParticleProgramFX;
        if(source.particlesImageIn != null && source.particlesImageOut != null) newSourceFX.setParticlesPatterns(source.particlesImageIn.path,source.particlesImageOut.path);
        
        if(source.particlesBlendEquationIn != null)  newSourceFX.particlesBlendEquationIn  = source.particlesBlendEquationIn;
        if(source.particlesBlendEquationOut != null) newSourceFX.particlesBlendEquationOut = source.particlesBlendEquationOut;

        return true;   
    });
}

EmitterFX.prototype.setFlarePattern = function(url)
{
    this.freeFlarePattern();
    if(url != null && url.length>0) this.flarePatternImage = imageCacheFX.createImage(url);
}

EmitterFX.prototype.setEmitterPattern = function(url)
{
    this.freeEmitterPattern();
    if(url != null && url.length>0) this.emitterPatternImage = imageCacheFX.createImage(url);
}

EmitterFX.prototype.freeFlarePattern = function() {
    if(this.flarePatternImage != null) imageCacheFX.deleteImage(this.flarePatternImage);
    this.flarePatternImage = null;             
}

EmitterFX.prototype.freeEmitterPattern = function() {
    if(this.emitterPatternImage != null) imageCacheFX.deleteImage(this.emitterPatternImage);
    this.emitterPatternImage = null; 
}

EmitterFX.prototype.applyParticlesForce = function(fx,fy)
{    
    this.particlesForcesAccumulator.x += fx;
    this.particlesForcesAccumulator.y += fy;
}

EmitterFX.prototype.createSource = function()
{
    var newSource = new SourceFX(this.context);                                                      
    newSource.programData = this.programData;
    
    this.sources.push(newSource);
    return newSource;
}
   
EmitterFX.prototype.deleteSource = function(e)
{
    var index = this.sources.indexOf(e);
    if(index == -1) return false;
    
    e.free();
    this.sources.splice(index,1);
    
    return true;
}

EmitterFX.prototype.clear = function()
{
    this.timeElapsed = 0.0;
    this.clearProgramData();
    this.flushInactiveParticles();
    
    this.sources.every(function(source) {
        source.clear();
        return true;
    });
}

EmitterFX.prototype.flushInactiveParticles = function()
{
    this.flushParticlesTimeLapsed = 0.0;
    
    this.sources.every(function(source) {
        source.flushInactiveParticles();
        return true;
    });
}

EmitterFX.prototype.free = function()
{
    this.sources.every(function(source)
    {
        source.free();
        return true;
    });
    
    this.sources = [];    
    this.freeFlarePattern();
    this.freeEmitterPattern();

    this.program = null;
}

EmitterFX.prototype.getSourcesNameList = function()
{
    var nameList = [];
    
    this.sources.every(function(source) {
        nameList.push(source.label);
        return true;
    });
    
    return nameList;  
}

EmitterFX.prototype.getSourceIndex = function(sourceObj) {
    return this.sources.indexOf(sourceObj);
}

EmitterFX.prototype.setSourceZ = function(source,zIndex)
{
    var currentIndex = this.sources.indexOf(source);
    this.sources.splice(zIndex,0,this.sources.splice(currentIndex,1)[0]);
}

EmitterFX.prototype.getActiveParticles = function()
{
    var count = 0;
    this.sources.every(function(source) {
        count += source.getActiveParticles();
        return true;
    });
    
    return count; 
}

EmitterFX.prototype.simulate = function(dt)
{   
    this.id = __emitterUniqueID++;
     
    this.oldPos.x = this.pos.x;
    this.oldPos.y = this.pos.y;
    this.pos.x += this.vel.x * dt;
    this.pos.y += this.vel.y * dt;
    
    this.refreshObservers();
    if(this.activeProgramFX && this.program != null) this.program.run(this,dt);
    if(this.updatingFunctor != null) this.updatingFunctor(this,dt);
    this.timeElapsed += dt;
    
    var velocityNrm = new vector2d(this.vel.x,this.vel.y); velocityNrm.normalize();
    this.slope = ((this.fixedSlope != null) ? this.fixedSlope : (new vector2d(0.0,1.0)).slope(velocityNrm));
    this.fixedSlope = null;
    
    var thisEmitterFX = this;
    this.sources.every(function(source)
    {   
        source.slope = thisEmitterFX.slope;
        source.vel.x = thisEmitterFX.vel.x;
        source.vel.y = thisEmitterFX.vel.y;
        source.pos.x = thisEmitterFX.pos.x;
        source.pos.y = thisEmitterFX.pos.y;
        source.oldPos.x = thisEmitterFX.oldPos.x;
        source.oldPos.y = thisEmitterFX.oldPos.y;
        
        source.applyParticlesForce(thisEmitterFX.particlesForcesAccumulator.x,thisEmitterFX.particlesForcesAccumulator.y);          
        source.simulate(dt);
        
        return true;
    }); 
    
    var flushParticleEachSeconds = 0.5;
    this.flushParticlesTimeLapsed += dt;
    if(this.flushParticlesTimeLapsed > flushParticleEachSeconds) this.flushInactiveParticles(); 
    
    if(this.enableFrameCollisions)
    {       
        if(this.pos.x < this.frameLeft) {
            this.pos.x = this.frameLeft;
            this.vel.x = -this.vel.x;
        }
        
        if(this.pos.y < this.frameTop) {
            this.pos.y = this.frameTop;
            this.vel.y = -this.vel.y;
        }
        
        if(this.pos.x > this.frameRight) {
            this.pos.x = this.frameRight;
            this.vel.x = -this.vel.x;
        }
        
        if(this.pos.y > this.frameBottom) {
            this.pos.y = this.frameBottom;
            this.vel.y = -this.vel.y;
        }
    }
                            
    this.particlesForcesAccumulator.x = 0.0;
    this.particlesForcesAccumulator.y = 0.0;                       
}

EmitterFX.prototype.render = function(renderingCtx)
{    
    this.sources.every(function(source)
    {                      
        source.render(renderingCtx);
        return true;
    });
    
    if(this.emitterPatternImage != null)
    {    
        renderingCtx.setOpacity(1.0);
        var patternSize = 2.0 * this.emitterPatternSize;
        renderingCtx.setBlendEquation(BLEND_EQUATION.SUBTRACTIVE);         
        renderingCtx.renderPattern(this.emitterPatternImage, this.pos.x,this.pos.y, patternSize,patternSize, this.slope + this.emitterPatternBias, this.emitterPatternTrimX,this.emitterPatternTrimY);      
    }                      
}

EmitterFX.prototype.renderFlare = function(renderingCtx)
{        
    if(this.flarePatternImage == null) return;
    
    renderingCtx.setOpacity(1.0);
    var flareSize = 2.0 * ((this.flarePatternFlickeringExp > 0.0) ? random(this.flarePatternSize * (1.0 - this.flarePatternFlickeringExp),this.flarePatternSize) : this.flarePatternSize); 
    renderingCtx.renderPattern(this.flarePatternImage, this.pos.x,this.pos.y, flareSize,flareSize, this.flarePatternBias, this.flarePatternTrimX,this.flarePatternTrimY);                   
}
