//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var SDK_version = 1.001;

function getRootSDK()
{
    var scripts = document.getElementsByTagName('script');
    var pathSDK = scripts[scripts.length-1].src.split('?')[0];
    var rootSDK = pathSDK.split('/').slice(0,-1).join('/')+'/';
    
    return rootSDK;
}

var rootSDK = getRootSDK();
function includeScriptSDK(src) {
    document.write("<script src='"+ rootSDK + "/" + src + "'></script>");
}

includeScriptSDK("sandbox.js");
includeScriptSDK("vector2D.js");
includeScriptSDK("utilities.js");
includeScriptSDK("FXSL.js");
includeScriptSDK("programFX.js");
includeScriptSDK("animation.js");
includeScriptSDK("renderingCtx.js");
includeScriptSDK("imageCache.js");
includeScriptSDK("globalsFX.js");
includeScriptSDK("particleFX.js");
includeScriptSDK("sourceFX.js");
includeScriptSDK("emitterFX.js");
includeScriptSDK("sceneFX.js");
includeScriptSDK("importerFX.js");
