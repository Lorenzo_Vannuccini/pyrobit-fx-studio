//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var Sandbox = function() {}

Sandbox.prototype.__sandboxConstruct = function(constructor, args) {
    // based on https://gist.github.com/alejandrolechuga/9381781
    function F() { return constructor.apply(this, args); }
    F.prototype = constructor.prototype;
    return new F();
}
 
Sandbox.prototype.clear = function() {
    this.__clearObject(this);
}

Sandbox.prototype.__clearObject = function(obj) {
    Object.keys(obj).forEach(function(key) { delete obj[key]; });
}

Sandbox.prototype.validatePropertyName = function(name)
{
    if(name == "eval") return false;
    else if(name != "Infinity" && !isNaN(name)) return false;
    
    return true;
}
   
Sandbox.prototype.run = function(script,errorHandler)
{
    "use strict";
    var dataShared = {};
    
    var this_Sandbox = this;
    var exclusionsList = [];
    var constructorArgs = [];

    script = "\"use strict\";\n"+script;
    
    Object.keys(this).every( function(name) {
        dataShared[name] = this_Sandbox[name];
        return true;
    });
   
    Object.getOwnPropertyNames(window).every(function(name) {           
        if(this_Sandbox.validatePropertyName(name)) exclusionsList.push(name);
        return true;
    });

    for(var name in window) {
        if(this_Sandbox.validatePropertyName(name) && exclusionsList.indexOf(name) == -1) exclusionsList.push(name);
    }

    constructorArgs.push(exclusionsList);
    constructorArgs.push(script);
    
    try {        
        this.__sandboxConstruct(Function,constructorArgs).apply(dataShared);        
    }
    catch(err)
    {
        this.__clearObject(dataShared);
        
        if(errorHandler != null) errorHandler(err);
        return false;    
    } 
    
    Object.keys(this).forEach(function(key) {
        this_Sandbox[key] = dataShared[key];
    });
    
    this.__clearObject(dataShared);
    return true;
}
