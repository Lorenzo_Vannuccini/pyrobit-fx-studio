//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var Animation = function(_functor,_time)
{
    if(_time == null) _time = 1000;
    this.frameRequest = null;
    
    this.stop();
    this.enableVSync(false);
    this.functor = _functor;
    this.setTimeInterval(_time);   
}

Animation.prototype.abortFrameRequest = function() {
    if(this.frameRequest != null) window.cancelAnimationFrame(this.frameRequest);
    this.frameRequest = null;
}

Animation.prototype.enableVSync = function(flag)
{ 
    var vSync_available = (window.requestAnimationFrame != null && window.cancelAnimationFrame != null);
    this.vSync = (vSync_available && flag);            
    
    return (this.vSync == flag);
}
               
Animation.prototype.run = function(_time)
{
    if(_time == null) _time = this.executionTime;
    
    this.setTimeInterval(_time);
    if(this.running) return;
    
    function animationLoop(thisAnimation)
    {
        var currTick = new Date().getTime();
        var timeDelta = ((thisAnimation.lastTick > 0.0) ? (currTick-thisAnimation.lastTick)/1000.0 : 0.0);
        thisAnimation.lastTick = currTick;
        
        thisAnimation.timeElapsed += timeDelta;
        thisAnimation.functor(timeDelta);
        
        thisAnimation.timer = setTimeout(function()
        {       
            if(!thisAnimation.vSync)
            {
                animationLoop(thisAnimation); 
                thisAnimation.frameRequest = null;
                 
            } else thisAnimation.frameRequest = window.requestAnimationFrame(function(){ animationLoop(thisAnimation); });
            
        }, thisAnimation.executionTime);
    }

    this.stop();
    this.running = true;
    animationLoop(this);
}

Animation.prototype.stop = function()
{   
    this.abortFrameRequest();  
    clearTimeout(this.timer);
    this.timer = null;  

    this.timeElapsed = 0.0;
    this.lastTick = 0.0;
    this.running = false;
}
    
Animation.prototype.setTimeInterval = function(_time) {
    this.executionTime = _time;
}
    
Animation.prototype.getTimeElapsed = function() {
    return this.timeElapsed;
} 
