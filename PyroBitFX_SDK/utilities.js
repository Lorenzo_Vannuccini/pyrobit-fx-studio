//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

function getElement(id) {
    return document.getElementById(id);
}

function setHTML(elementID, code) {
    getElement(elementID).innerHTML = code;
}

function injectHTML(elementID, code) {
    getElement(elementID).innerHTML += code;
}
 
function getStyle(id) {
    var element = getElement(id);
    return ((element != null) ? element.style : null);
}

function deleteElement(id) {
    var element = document.getElementById(id);
    if(element != null) element.parentNode.removeChild(element);
}

function pageZoom() {
    return window.devicePixelRatio;
}
    
function pageWidth() {
    return Math.max(document.body.scrollWidth, document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth);
}

function pageHeight() {
    return Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
}

function clientWidth() {
    return window.innerWidth;
}

function clientHeight() {
    return window.innerHeight;
}

var cursorDown = false;            
var cursorAbsoluteX = 0.0, cursorAbsoluteY = 0.0;
document.body.onmouseup = function() { cursorDown = false; }
document.body.onmousedown = function() { cursorDown = true; }
document.onmousemove = function(e) { cursorAbsoluteX = e.clientX; cursorAbsoluteY = e.clientY; }

document.addEventListener("touchstart",document.body.onmouseup,true);
document.addEventListener("touchend",document.body.onmousedown,true);
document.addEventListener("touchmove",document.onmousemove,true);

var PIover180 = Math.PI / 180.0;
var PIover360 = Math.PI / 360.0;
     
function abs(x) {
    return ((x < 0.0) ? -x : x);
}

function clamp(x,_min,_max) {
    if(x < _min) return _min;
    else if(x > _max) return _max;
    else return x;
}

function random(_min,_max) {
    
    if(_min == _max) return _min;
    
    if(_min > _max)
    {
        var tmp = _min;
        _min = _max;
        _max = tmp;
    }

    return _min + Math.random() * (_max - _min);
}

function min(a,b) {
    return (a < b ? a : b);
}

function max(a,b) {
    return (a > b ? a : b);
}

function mix(x,y,a) {   // linear interpolation
    return x*(1.0-a) + y*a;
}

function smoothstep(edge0,edge1,x) { // Hermite interpolation
    var t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
    return t * t * (3.0 - 2.0 * t);
}
  
function loadImageAsync(node,url,_set,_completion)
{
    var img = new Image();
    
    img.onload = function () {
        node.appendChild(this);
        if(_completion != null) _completion(this);
    }
    
    img.src = url;
    if(_set != null) _set(img);
}

function loadScript(url,callback)
{    
    var script = document.createElement('SCRIPT');
    script.type = 'text/javascript';
    script.charset = 'UTF-8';
    script.src = url;
    
    // script.onreadystatechange = callback;
    script.onload = callback;
    
    var index = document.getElementsByTagName('SCRIPT')[0];
    index.parentNode.insertBefore(script, index);
}

function playSound(url,_loop)
{
    if(typeof Audio == "undefined") return; // playSound() does not work in safari

    var newSound = new Audio(url);
    newSound.loop = ((_loop != null) ? _loop : false);

    newSound.addEventListener("loadedmetadata", function() {
        this.currentTime = random(0.0,this.duration); 
    });

    newSound.addEventListener("ended",function()
    {
        if(!this.loop) return;
        
        this.currentTime = 0;
        this.play();
    },  false);

    function volumeFadeIn(q)
    {
        var InT = 0;
        var setVolume = 1.0;
        var speed = 0.005;
        q.volume = InT;
        
        var eAudio = setInterval(function()
        {
            InT += speed;
            q.volume = InT.toFixed(1);
            
            if(InT.toFixed(1) >= setVolume) clearInterval(eAudio);
        },5);
    }
    
    newSound.volume = 0.0;
    volumeFadeIn(newSound);
    newSound.play();
    
    return newSound;
}  