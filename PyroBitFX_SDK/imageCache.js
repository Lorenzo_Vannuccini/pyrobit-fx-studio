//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var ImageCache = function()
{   
    this.images = {};
    this.size = 0;
}

ImageCache.prototype.createImage = function(src)
{
    var cachedImage = this.images[src];
    
    if(cachedImage == null)
    {                         
       cachedImage = this.images[src] = new Image();
       
       cachedImage.width = cachedImage.height = 0;
       cachedImage.style.opacity = 0.0;
       cachedImage.referenceCount = 0;
       cachedImage.path = src;
       cachedImage.src = src;
       ++this.size;
    }

    ++cachedImage.referenceCount;
    return cachedImage; 
}

ImageCache.prototype.deleteImage = function(img)
{
    var cachedImage = this.images[img.path];
    if(cachedImage == null) return false;

    if(--cachedImage.referenceCount <= 0) {
        this.images[img.path] = null;
        --this.size;
    }
      
    return true; 
}

ImageCache.prototype.getSourceList = function()
{
    var sourceList = [];
    for(var src in this.images) nameList.push(src);
    
    return sourceList; 
}

ImageCache.prototype.free = function()
{
    for(var key in this.images) this.images[key] = null;
    this.size = 0;  
}
