//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var ParticleFX = function(context,imgIn,imgOut,x,y)
{    
    this.id = 0;
    this.context = context;
    
    this.locate(x,y);                
    this.imgIn = imgIn;
    this.imgOut = imgOut;
    
    this.program = null;
    this.programInstanceData = {}; // program data for this particle instance
        
    this.decayExp = 0.5; 
    this.timeElapsed = 0.0;                                                                                                           
    this.torque = this.spin = 0.0;
    this.life = this.lifeMax = 1.0;
    this.size = 40.0; this.sizeVariation = 1.0;
    this.frictionExp = this.torqueFrictionExp = 0.0;
    this.transitionStartExp = this.transitionFadeExp = 0.5;
    
    this.blendEquationIn = this.blendEquationOut = BLEND_EQUATION.SUBTRACTIVE;
}
    
ParticleFX.prototype.locate = function(x,y,z)
{
    if(z == null) z = 1.0;
     
    this.pos = new vector2d(x,y);
    this.vel = new vector2d(0.0);        
}
    
ParticleFX.prototype.simulate = function(dt)
{  
    this.id = __particleUniqueID++;
    
    if(this.life <= 0.0) return;
     
    if(this.program != null) {
        this.program.instanceData = this.programInstanceData;
        this.program.run(this,dt);
    } 
    
    var dtSquared = dt * dt;
    
    // Forward Euler Integration
    this.pos.x += this.vel.x * dt; 
    this.pos.y += this.vel.y * dt;
    
    this.timeElapsed += dt;            
    this.spin += this.torque * dt; 
    this.life  = max(this.life - dt, 0.0);
    
    this.vel.x  -= this.vel.x  * this.frictionExp       * dtSquared;
    this.vel.y  -= this.vel.y  * this.frictionExp       * dtSquared;
    this.torque -= this.torque * this.torqueFrictionExp * dtSquared;
}
    
ParticleFX.prototype.applyForce = function(fx,fy)
{  
    var deltaFactor = 40.0;
     
    this.vel.x += fx * deltaFactor;
    this.vel.y += fy * deltaFactor;
}

ParticleFX.prototype.render = function(renderingCtx)
{   
    if(this.life <= 0.0) return;
     
    var lifeSqrd = smoothstep(0.0, this.lifeMax * this.decayExp, this.life);
    var transitionStep0 = this.lifeMax * (1.0 - this.transitionStartExp);
    var transitionStep1 = transitionStep0 * (0.999 - this.transitionFadeExp);
    var derivativeSize = ((lifeSqrd > 0.0) ? (2.0 * mix(this.size * this.sizeVariation,this.size, this.life/this.lifeMax)) : 0.0);

    var particleOUT_opacity = smoothstep(transitionStep0,transitionStep1,this.life) * lifeSqrd;
    var particleIN_opacity = lifeSqrd - particleOUT_opacity;
    
    renderingCtx.setOpacity(particleIN_opacity);
    renderingCtx.setBlendEquation(this.blendEquationIn);
    renderingCtx.renderPattern(this.imgIn, this.pos.x,this.pos.y,derivativeSize,derivativeSize,this.spin, 0.0,0.0);
    
    renderingCtx.setOpacity(particleOUT_opacity);
    renderingCtx.setBlendEquation(this.blendEquationOut);
    renderingCtx.renderPattern(this.imgOut,this.pos.x,this.pos.y,derivativeSize,derivativeSize,this.spin, 0.0,0.0);
}
