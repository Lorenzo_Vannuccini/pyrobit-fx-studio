//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
                  
var BLEND_EQUATION =
{
    ADDITIVE:        "lighter",
    SUBTRACTIVE:     "source-over",
    MULTIPLICATIVE:  "multiply",
    RECIPROCAL:      "screen",
    OVERLAY:         "overlay",
    LIGHTEN:         "lighten",
    DARKEN:          "darken"    
};

var RenderingCtx = function()
{    
    this.canvasBlur = document.createElement('canvas');
    this.contextBlur = this.canvasBlur.getContext("2d");
    
    var fullscreenCanvas = document.createElement('canvas');
    fullscreenCanvas.renderingCtxReference = this;
    
    fullscreenCanvas.style.cssText = "pointer-events: none;\n" +
                                     "-webkit-touch-callout: none;\n" +
                                     "-webkit-user-select: none;\n" +
                                     "-khtml-user-select: none;\n" +
                                     "-moz-user-select: none;\n" +
                                     "-ms-user-select: none;\n" +
                                     "user-select: none;\n";
                               
    fullscreenCanvas.style.position = "fixed";
    
    fullscreenCanvas.style.width = fullscreenCanvas.style.height = "100%";
    fullscreenCanvas.style.bottom = fullscreenCanvas.style.right = 0;
    fullscreenCanvas.style.left = fullscreenCanvas.style.top = 0;
    
    document.body.appendChild(fullscreenCanvas); 
    this.bindCanvas(fullscreenCanvas);    
    
    this.enableFocus(false);
    this.setScaleFactor(1.0); 
    this.refreshSize();
    
    this.clear();
    this.setMotionBlur(1.0);
    this.enableImageResampling(true);
    this.__setCanvasContextImageResampling(this.contextBlur,false);
}

RenderingCtx.prototype.enableFocus = function(flag)
{
    var cssFlag = (flag ? "auto" : "none");
    
    this.canvas.style["pointer-events"] = cssFlag;
    this.canvas.style["-webkit-touch-callout"] = cssFlag;
    this.canvas.style["-webkit-user-select"] = cssFlag;
    this.canvas.style["-khtml-user-select"] = cssFlag;
    this.canvas.style["-moz-user-select"] = cssFlag;
    this.canvas.style["-ms-user-select"] = cssFlag;
    this.canvas.style["user-select"] = cssFlag;    
}

RenderingCtx.prototype.freeCanvas = function()
{
    if(this.canvas != null && this.canvas.renderingCtxReference == this) this.canvas.parentNode.removeChild(this.canvas);
    this.context = this.canvas = null;        
}

RenderingCtx.prototype.free = function()
{
    this.clear();
    this.freeCanvas();
    this.contextBlur = this.canvasBlur = null;        
}

RenderingCtx.prototype.__setCanvasContextImageResampling = function(ctx,flag)
{
    ctx.webkitImageSmoothingEnabled = flag;
//  ctx.mozImageSmoothingEnabled = flag; // deprecated
    ctx.msImageSmoothingEnabled = flag;
    ctx.imageSmoothingEnabled = flag;
}

RenderingCtx.prototype.enableImageResampling = function(flag) {
    this.__setCanvasContextImageResampling(this.context,flag);
}

RenderingCtx.prototype.refreshSize = function()
{   
    var clientW = this.canvas.clientWidth;
    var clientH = this.canvas.clientHeight;
                      
    if(this.canvas.width != clientW || this.canvas.height != clientH) 
    {
        this.canvas.width  = this.canvasBlur.width  = clientW;
        this.canvas.height = this.canvasBlur.height = clientH;  
    } 
}

RenderingCtx.prototype.getWidth = function() {
    return ((this.canvas != null) ? this.canvas.width : 0.0);       
}

RenderingCtx.prototype.getHeight = function() {
    return ((this.canvas != null) ? this.canvas.height : 0.0);       
}

RenderingCtx.prototype.screenToClient = function(x,y)
{
    var clientRect = this.canvas.getBoundingClientRect(); 
    var clientH = this.canvas.clientHeight;
    var clientX = clientRect.left;
    var clientY = clientRect.top;
    
    return new vector2d((x - clientX)/this.scaleFactor, (clientH - y + clientY)/this.scaleFactor);                  
}

RenderingCtx.prototype.clientToScreen = function(x,y)
{
    var clientRect = this.canvas.getBoundingClientRect(); 
    var clientH = this.canvas.offsetHeight;
    var clientX = clientRect.left;
    var clientY = clientRect.top;
    
    return new vector2d((x + clientX)*this.scaleFactor, -(y*this.scaleFactor - clientH - clientY));                  
}

RenderingCtx.prototype.setZ = function(zIndex) {
    this.canvas.style.zIndex = zIndex;
}

RenderingCtx.prototype.getZ = function() {
    return this.canvas.style.zIndex;
}

RenderingCtx.prototype.setScaleFactor = function(factor) {
    this.scaleFactor = factor;
}

RenderingCtx.prototype.getScaleFactor = function() {
    return this.scaleFactor;
}

RenderingCtx.prototype.bindCanvas = function(canvasObj)
{
    if(this.canvas == canvasObj) return;
    
    this.freeCanvas();
    this.canvas = canvasObj;
    this.context = this.canvas.getContext("2d");
    
    this.refreshSize();
}

RenderingCtx.prototype.setMotionBlur = function(exp)
{
    if(exp <= 0.0) this.clearMotionBlur();
    this.blurExp = clamp(exp,0.0,1.0);
}

RenderingCtx.prototype.clearMotionBlur = function() {
    if(this.blurExp > 0.0) this.contextBlur.clearRect(0,0,this.canvasBlur.width,this.canvasBlur.height);
}

RenderingCtx.prototype.prepareMotionBlur = function()
{
    if(this.blurExp <= 0.0) return;
      
    this.contextBlur.globalCompositeOperation = "copy";
    this.contextBlur.drawImage(this.canvas,0,0,this.canvasBlur.width,this.canvasBlur.height);
}

RenderingCtx.prototype.applyMotionBlur = function()
{
    if(this.blurExp <= 0.0) return;
    
    this.setOpacity(0.5 * this.blurExp);
    this.setBlendEquation(BLEND_EQUATION.SUBTRACTIVE);
    this.context.drawImage(this.canvasBlur,0,0,this.canvas.width,this.canvas.height);
}

RenderingCtx.prototype.clear = function(r,g,b,a)
{   
    if(this.context == null || this.canvas.width<1 || this.canvas.height<1) return;
    this.context.clearRect(0,0,this.canvas.width,this.canvas.height);
        
    if(r != null && g != null && b != null)
    {
        if(a == null) a = 1.0;
        this.context.fillStyle = "rgba("+ r +","+ g +","+ b +"," + a +")";
        this.context.fillRect(0,0,this.canvas.width,this.canvas.height);
    }
}

RenderingCtx.prototype.setOpacity = function(alpha) {
    this.context.globalAlpha = alpha;
}

RenderingCtx.prototype.setBlendEquation = function(blendMode) {
    if(this.context.globalCompositeOperation != blendMode) this.context.globalCompositeOperation = blendMode;
}

RenderingCtx.prototype.renderPattern = function(img,x,y,w,h,rotDeg,trimX,trimY)
{    
    if(this.context == null || img == null) return;
    
    if(this.scaleFactor != 1.0)
    {
        x *= this.scaleFactor;
        y *= this.scaleFactor;
        w *= this.scaleFactor;
        h *= this.scaleFactor;
    }
    
    var halfW = w*0.5, halfH = h*0.5;
    this.context.translate(x, this.canvas.height-y);
    if(rotDeg != 0.0 && rotDeg != null) this.context.rotate(rotDeg * PIover180);
    this.context.translate(-halfW - trimX*halfW, -halfH + trimY*halfH);

    try {
        this.context.drawImage(img,0,0,w,h);
    }
    catch(err) { // prevents drawImage() for rising critical errors due to broken images
        //console.log(err); // catch critical error bubble 
    }
    
    this.context.setTransform(1.0,0.0,0.0,1.0,0.0,0.0); // load identity matrix
}
