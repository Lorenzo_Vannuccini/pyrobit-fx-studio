//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

function FX_fileHeader()
{
    var header = "/*FX_vr:" + SDK_version.toFixed(3);
    for(var i=0,l=header.length; i<18-l; ++i) header += " ";
    header += "*/";
    
    return header;
}

function FX_validateFileHeader(content)
{
    if(content.length < 20) return false;
    if(content.substring(0,8) != "/*FX_vr:") return false;
    
    var headerVersion = parseFloat(content.substring(8,18));
    return (headerVersion == SDK_version);
}

function FX_fileData(content) {
    return content.substring(59,content.length-1);
}

function preloadFX(path,callback)
{   
    if(__FX_DATA_CACHE[path] != null)
    {
        if(callback != null) callback(path); 
        return;
    } 
    
    loadScript(path, function()
    {                                               
        __FX_DATA_CACHE[path] = __FX_DATA_CACHE["__cache_import_fx"];
        __FX_DATA_CACHE["__cache_import_fx"] = null;
        
        if(callback != null) callback(path);
    }); 
}
