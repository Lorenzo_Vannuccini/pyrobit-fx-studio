//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var SourceFX = function(context)
{   
    this.id = 0;
    this.context = context;
        
    this.label = "";
    this.delay = 0.0;
    this.loop = false;
    this.life = Infinity;
    this.particlesDecayExp = 0.5;
    this.particlesInertiaExp = 0.25;
    this.spanMin = new vector2d(0.0,0.0);
    this.spanMax = new vector2d(0.0,0.0);
    this.particlesTransitionFadeExp = 0.5;
    this.particlesTransitionStartExp = 0.25;
        
    this.particlesSpeedMin = 0.0;
    this.particlesSpeedMax = 100.0;
    this.spawningTimeMin = this.spawningTimeMax = 0.02;
    this.spawningAmountMin = this.spawningAmountMax = 1;
    this.particlesLifeMin = this.particlesLifeMax = 1.0;
    this.particlesSizeMin = this.particlesSizeMax = 20.0;
    this.particlesTorqueMin = this.particlesTorqueMax = 0.0;
    this.particlesVelocityBiasMin = this.particlesRotationMin = -180.0;
    this.particlesVelocityBiasMax = this.particlesRotationMax = +180.0;
    this.particlesSizeVariationMin = this.particlesSizeVariationMax = 0.0;
    this.particlesFrictionExp = this.particlesTorqueFrictionExp = 0.0;
    this.particlesAcceleration = new vector2d(0.0);
    
    this.activeParticleProgramFX = true;
    this.activeProgramFX = true;
    this.particleProgram = null;
    this.programData = null;
    this.program = null;
    
    this.pos = new vector2d();
    this.oldPos = new vector2d(this.pos.x,this.pos.y);
    
    this.slope = 0.0;    
    this.vel = new vector2d(1.0,0.0);
    this.particlesForcesAccumulator = new vector2d(0.0);
   
    this.particles = [];
    this.spawnTimer = 0.0;
    this.timeElapsed = 0.0;
    this.sourceTimeElapsed = 0.0;
    
    this.particlesImageIn = this.particlesImageOut = null;
    this.particlesBlendEquationIn = this.particlesBlendEquationOut = BLEND_EQUATION.SUBTRACTIVE;
}

SourceFX.prototype.setProgram = function(source)
{
    if(source == null) {
        this.program = null;
        return true;
    }
     
    this.program = new programFX("SOURCE_PROGRAM");
    this.program.setErrorHandler( function(err){ alert(err); });
    this.program.data = this.programData; // shared data between same context programs
    
    return this.program.compile(source);
}

SourceFX.prototype.setParticleProgram = function(source)
{
    if(source == null) {
        this.particleProgram = null;
        return true;
    }
    
    this.particleProgram = new programFX("PARTICLE_PROGRAM");
    this.particleProgram.setErrorHandler( function(err){ alert(err); });
    this.particleProgram.data = this.programData; // shared data between same context programs
    
    return this.particleProgram.compile(source);
}

SourceFX.prototype.clear = function()
{
    this.particles = [];
    this.timeElapsed = 0.0;
    this.sourceTimeElapsed = 0.0;
}

SourceFX.prototype.setParticlesPatternIn = function(url) {
    this.setParticlesPatterns(url,(this.particlesImageOut != null ? this.particlesImageOut.path : null));
}

SourceFX.prototype.setParticlesPatternOut = function(url) {
    this.setParticlesPatterns((this.particlesImageIn != null ? this.particlesImageIn.path : null),url);
}

SourceFX.prototype.setParticlesPatterns = function(particle_in_url,particle_out_url)
{
    if(particle_in_url == null  || particle_in_url.length<1) {
        this.particlesBlendEquationIn = this.particlesBlendEquationOut;
        particle_in_url = particle_out_url;
    }
     
    if(particle_out_url == null || particle_out_url.length<1) {
        this.particlesBlendEquationOut = this.particlesBlendEquationIn;
        particle_out_url = particle_in_url;
    } 
    
    this.freeParticlesPatterns();
    this.particlesImageIn = imageCacheFX.createImage(particle_in_url);
    this.particlesImageOut = imageCacheFX.createImage(particle_out_url);
}

SourceFX.prototype.freeParticlesPatterns = function()
{   
    if(this.particlesImageOut != null) imageCacheFX.deleteImage(this.particlesImageOut);
    if(this.particlesImageIn != null) imageCacheFX.deleteImage(this.particlesImageIn);
    this.particlesImageIn = this.particlesImageOut = null;             
    this.clear();
}

SourceFX.prototype.free = function() {
    this.freeParticlesPatterns();
    this.particleProgram = null;
    this.program = null;
}
                      
SourceFX.prototype.spawnParticle = function(x,y, vx,vy)
{    
    if(vx == null) vx = 0.0;
    if(vy == null) vy = 0.0;
    
    var span = new vector2d();
    span.x = random(this.spanMin.x,this.spanMax.x);
    span.y = random(this.spanMin.y,this.spanMax.y);
    span.rotate(this.slope);

    var newParticle = new ParticleFX(this.context,this.particlesImageIn,this.particlesImageOut, x+span.x,y+span.y);
    newParticle.programData = this.programData; // shared data between same context programs
    
    newParticle.blendEquationIn = this.particlesBlendEquationIn;
    newParticle.blendEquationOut = this.particlesBlendEquationOut;
    
    newParticle.vel.x = 0.0;
    newParticle.vel.y = 1.0;    
    var speed = random(this.particlesSpeedMin,this.particlesSpeedMax);
    newParticle.vel.rotate(-this.slope + random(this.particlesVelocityBiasMin,this.particlesVelocityBiasMax));
    newParticle.vel.mult(speed);
           
    newParticle.torque = random(this.particlesTorqueMin,this.particlesTorqueMax);
    newParticle.spin = this.slope + random(this.particlesRotationMin,this.particlesRotationMax);
    
    newParticle.size = random(this.particlesSizeMin,this.particlesSizeMax);
    newParticle.sizeVariation = random(this.particlesSizeVariationMin,this.particlesSizeVariationMax);
                      
    newParticle.decayExp = this.particlesDecayExp;
    newParticle.frictionExp = this.particlesFrictionExp;
    newParticle.torqueFrictionExp = this.particlesTorqueFrictionExp;
    newParticle.life = random(this.particlesLifeMin,this.particlesLifeMax);
              
    newParticle.transitionStartExp = this.particlesTransitionStartExp;
    newParticle.transitionFadeExp  = this.particlesTransitionFadeExp;

    newParticle.lifeMax = newParticle.life;
    newParticle.vel.x += vx;
    newParticle.vel.y += vy;

    this.particles.push(newParticle);
}

SourceFX.prototype.applyParticlesForce = function(fx,fy)
{    
    this.particles.every(function(particle)
    {
        particle.applyForce(fx,fy);
        return true;
    });
}

SourceFX.prototype.getActiveParticles = function() {
    return this.particles.length;
}

SourceFX.prototype.flushInactiveParticles = function()
{
    for(var i = this.particles.length; i-- > 0;) {      
        if(this.particles[i].life <= 0.0) this.particles.splice(i,1);
    }
}

SourceFX.prototype.simulate = function(dt)
{
    this.id = __sourceUniqueID++;
    if(this.activeProgramFX && this.program != null) this.program.run(this,dt);
    
    this.sourceTimeElapsed += dt; 
    if(this.loop) this.sourceTimeElapsed = this.sourceTimeElapsed % (this.delay+this.life);
    var active = (this.sourceTimeElapsed >= this.delay && this.sourceTimeElapsed < this.delay+this.life);
    
    if(active)
    {
        this.spawnTimer += dt;
        this.timeElapsed += dt; 
        
        if(this.spawningTime == null) {
            this.spawningTime   = random(this.spawningTimeMin, this.spawningTimeMax);
            this.spawningAmount = Math.floor(random(this.spawningAmountMin,this.spawningAmountMax + 0.5));
        } 
         
        for(var delta = this.spawningTime, max = this.spawnTimer; (this.spawnTimer >= delta); this.spawnTimer-=delta)
        {
            this.spawningTime = null;
            var interpoleRate = this.spawnTimer / max;
            var emitPos = new vector2d(mix(this.oldPos.x,this.pos.x,interpoleRate), mix(this.oldPos.y,this.pos.y,interpoleRate));
                    
            for(var i=0; i<this.spawningAmount; ++i) this.spawnParticle(emitPos.x,emitPos.y, this.vel.x * this.particlesInertiaExp, this.vel.y * this.particlesInertiaExp); 
        }
        
    } else this.spawnTimer = 0.0;
    
    var thisSource = this;
    this.particles.every(function(particle)
    {   
        particle.applyForce(thisSource.particlesAcceleration.x * dt,thisSource.particlesAcceleration.y * dt);              
        particle.program = ((thisSource.activeParticleProgramFX) ? thisSource.particleProgram : null);
        particle.simulate(dt);
                
        return true;
    });
}

SourceFX.prototype.render = function(renderingCtx)
{        
    this.particles.every(function(particle)
    {
        particle.render(renderingCtx);
        return true;
    });
}
