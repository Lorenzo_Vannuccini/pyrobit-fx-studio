//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var SceneFX = function(simulationUpdateCallback)
{
    this.running = true;
    var thisScene = this;    
    this.animation = new Animation(function(dt) {
        if(thisScene.running) thisScene.computeFrame(dt);
    });  
    
    this.updatingFunctor = simulationUpdateCallback;
    
    this.renderingContext = new RenderingCtx();   
    this.enableImageResampling(false);
    this.enableMotionBlur(true);
    this.enableVSync(true);
    
    this.setScaleFactor(1.0);   
    this.programData = {};
    this.observers = {};  
    this.emitters = [];
    
    this.activeParticles = 0; 
    this.fpsTimeElapsed = 0.0;
    this.fpsCounter = 0;    
    this.fps = 0.0;
}

SceneFX.prototype.setSimulation = function(simulationUpdateCallback) {
    this.updatingFunctor = simulationUpdateCallback;
}

SceneFX.prototype.enableFocus = function(flag) {
    this.renderingContext.enableFocus(flag);
}

SceneFX.prototype.enableMotionBlur = function(flag) {
    this.renderingContext.setMotionBlur(flag ? 1.0 : 0.0);
}

SceneFX.prototype.enableImageResampling = function(flag) {
    this.renderingContext.enableImageResampling(flag);    
}

SceneFX.prototype.enableVSync = function(flag) {
    this.animation.enableVSync(flag);    
}

SceneFX.prototype.getWidth = function() {
    return this.renderingContext.getWidth();       
}

SceneFX.prototype.getHeight = function() {
    return this.renderingContext.getHeight();       
}

SceneFX.prototype.screenToClient = function(x,y) {
    return this.renderingContext.screenToClient(x,y);                 
}

SceneFX.prototype.clientToScreen = function(x,y) {
    return this.renderingContext.clientToScreen(x,y);     
}

SceneFX.prototype.setZ = function(zIndex) {
    this.renderingContext.setZ(zIndex);
}

SceneFX.prototype.getZ = function() {
    return this.renderingContext.getZ();
}

SceneFX.prototype.setScaleFactor = function(factor) {
    this.renderingContext.setScaleFactor(factor);
}

SceneFX.prototype.getScaleFactor = function() {
    return this.renderingContext.getScaleFactor();
}

SceneFX.prototype.bindCanvas = function(canvasObj) {
    this.renderingContext.bindCanvas(canvasObj);
}

SceneFX.prototype.resume = function()
{
    this.running = true;
    if(this.emitters.length > 0 && !this.animation.running) this.animation.run(0); 
}

SceneFX.prototype.pause = function()
{
    this.running = false;
    this.animation.stop(); 
}

SceneFX.prototype.createEmitter = function(url,initializationHandler)
{   
    var newEmitterFX = new EmitterFX(this,url,initializationHandler);
    this.emitters.push(newEmitterFX);
     
    if(this.running && !this.animation.running) this.animation.run(0);
    
    return newEmitterFX;
}

SceneFX.prototype.createEmitterCopy = function(emitter,simulationUpdateCallback) // TODO: refactor duplicated code with createEmitter()
{   
    var newEmitterFX = emitter.copy(this);
    
    newEmitterFX.setSimulation(simulationUpdateCallback);
    this.emitters.push(newEmitterFX);
    
    if(this.running && !this.animation.running) this.animation.run(0);
    
    return newEmitterFX;
}

SceneFX.prototype.deleteEmitter = function(emitterObj)
{
    var emitterIndex = this.emitters.indexOf(emitterObj);
    if(emitterIndex == -1) return false;
    
    emitterObj.free();
    this.emitters.splice(emitterIndex,1);
    
    if(this.running && this.animation.running && this.emitters.length <= 0) {
        this.renderingContext.clear();
        this.animation.stop();
    } 

    return true;
}

SceneFX.prototype.setObserver = function(eventID,eventHandler)
{
    if(eventHandler != null) this.observers[eventID] = eventHandler;
    else delete this.observers[eventID];    
}

SceneFX.prototype.hasObserver = function(eventID) {
    return (this.observers[eventID] != null);   
}

SceneFX.prototype.clearObservers = function() {
    this.observer = {};
}

SceneFX.prototype.computeFrame = function(dt)
{   
    this.updateFrameData(dt);
    
    var maxFrameRateDropAllowed = 20.0; // below 20 fps don't compensate for time gap, slow animation down instead 
    dt = clamp(dt,0.00001,1.0/maxFrameRateDropAllowed);

    if(this.updatingFunctor != null) this.updatingFunctor(this,dt);
    
    this.renderingContext.setBlendEquation(BLEND_EQUATION.SUBTRACTIVE);             
    this.renderingContext.refreshSize();
    this.renderingContext.clear();
    
    var clientW = this.renderingContext.getWidth();
    var clientH = this.renderingContext.getHeight();
    
    var thisScene = this;
    __emitterUniqueID = __sourceUniqueID = __particleUniqueID = 0;
     
    this.emitters.every( function(emitter)
    {   
        var localObserversIDs = [];
        var localProgramDataIDs = [];
        
        for(var key in thisScene.observers)
        {
            if(emitter.hasObserver(key)) continue; // allow global observers to be overridden by local ones
            
            emitter.setObserver(key,thisScene.observers[key]);
            localObserversIDs.push(key);
        }
        
        for(var key in thisScene.programData)
        {
            if(emitter.programData[key] != null) continue; // allow programs global data to be overridden by local
            
            emitter.programData[key] = thisScene.programData[key];
            localProgramDataIDs.push(key);
        } 

        emitter.frameTop = ((thisScene.frameTop != null) ? thisScene.frameTop : 0.0);
        emitter.frameLeft = ((thisScene.frameLeft != null) ? thisScene.frameLeft : 0.0);     
        emitter.frameRight = ((thisScene.frameRight != null) ? thisScene.frameRight : (clientW / thisScene.renderingContext.scaleFactor));
        emitter.frameBottom = ((thisScene.frameBottom != null) ? thisScene.frameBottom : (clientH / thisScene.renderingContext.scaleFactor));
              
        emitter.simulate(dt);
        emitter.render(thisScene.renderingContext);
        
        // remove local observers / program data
        localProgramDataIDs.every( function(key) {
             delete emitter.programData[key];
             return true;
        });
        
        localObserversIDs.every( function(key) {
             emitter.setObserver(key,null);
             return true;
        });
                          
        return true;
    });
    
    this.renderingContext.applyMotionBlur();            
    this.renderingContext.prepareMotionBlur();
      
    this.renderingContext.setBlendEquation(BLEND_EQUATION.ADDITIVE);    
    this.emitters.every(function(emitter)
    {
        emitter.renderFlare(thisScene.renderingContext);
        return true;
    });       
}

SceneFX.prototype.updateFrameData = function(dt)
{
    ++this.fpsCounter;
    this.fpsTimeElapsed += dt;
    
    if(this.fpsTimeElapsed >= 0.5)
    {
        this.fpsTimeElapsed = 0.0;
        this.fps = 2.0 * this.fpsCounter;
        this.fpsCounter = 0;
        
        var thisScene = this;
        this.activeParticles = 0;
        this.emitters.every(function(emitter) {
            thisScene.activeParticles += emitter.getActiveParticles();
            return true;
        });
    }                
}

SceneFX.prototype.setProgramData = function(key,value)
{   
    if(value != null) this.programData[key] = value;
    else delete this.programData[key]; 
}

SceneFX.prototype.clearProgramData = function() {
    this.programData = {};
}

SceneFX.prototype.clear = function()
{   
    this.clearProgramData();
    this.renderingContext.clear();
        
    this.emitters.every(function(emitter)
    {
        emitter.clear();
        return true;
    });    
}

SceneFX.prototype.free = function()
{   
    this.renderingContext.free();
    
    this.emitters.every(function(emitter)
    {
        emitter.free();
        return true;
    });
    
    this.emitters = [];
    if(this.running && this.animation.running) this.animation.stop();  
}
