//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var vector2d = function(x,y)
{
    if(x == null) x = 0.0;
    if(y == null) y = x;
    
    this.x = x;
    this.y = y;
}

vector2d.prototype.sum = function(v)
{
    this.x += ((v.x != null) ? v.x : v);
    this.y += ((v.y != null) ? v.y : v);
    
    return this;
}

vector2d.prototype.sub = function(v)
{
    this.x -= ((v.x != null) ? v.x : v);
    this.y -= ((v.y != null) ? v.y : v);
    
    return this;
}

vector2d.prototype.mult = function(v)
{
    this.x *= ((v.x != null) ? v.x : v);
    this.y *= ((v.y != null) ? v.y : v);
    
    return this;
}

vector2d.prototype.div = function(v)
{
    this.x /= ((v.x != null) ? v.x : v);
    this.y /= ((v.y != null) ? v.y : v);
    
    return this;
}

vector2d.prototype.dot = function(v) {
    return this.x * v.x + this.y * v.y;
}

vector2d.prototype.cross = function(v) {
    return v.x * this.y - v.y * this.x;
}

vector2d.prototype.squaredLength = function() {
    return this.dot(this);
}

vector2d.prototype.length = function() {
    return Math.sqrt(this.squaredLength());
}

vector2d.prototype.normalize = function()
{
    var squaredLenght = this.squaredLength();
    if(squaredLenght == 0.0) return this;
    
    return this.div(Math.sqrt(squaredLenght));
}

vector2d.prototype.slope = function(v)
{
    var dot = clamp(this.dot(v),-1.0,1.0);
    if(dot == 0.0) return 0.0;
    
    var cross = this.cross(v);
    var deg = Math.acos(dot) / PIover180;
    
    return (cross < 0.0) ? -deg : deg;   
}

vector2d.prototype.rotate = function(deg)
{   
    var px = this.x;
    var py = this.y;
    
    var radians = deg * PIover180;     
    var cosRad = Math.cos(radians), sinRad = Math.sin(radians);

    this.x = px * cosRad - py * sinRad;
    this.y = py * cosRad + px * sinRad;

    return this;
}

