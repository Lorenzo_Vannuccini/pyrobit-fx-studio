//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var __inputHandlerLookupTable = [];
var __inputUpdaterLookupTable = [];

function __inputEventHandler(inputObj,round)
{
    if(inputObj.type == "checkbox")
    {
        if(inputObj.value      == "true")  inputObj.value = 1.0;
        else if(inputObj.value == "false") inputObj.value = 0.0;
        
        inputObj.value = (inputObj.value > 0.0 ? 0.0 : 1.0);
    } 
    
    if(round) inputObj.value = Math.round(inputObj.value); 
    if(isNaN(parseFloat(inputObj.value))) inputObj.value = 0.0;
    inputObj.value = clamp(parseFloat(inputObj.value),inputObj.min,inputObj.max);
    
    var output = __inputHandlerLookupTable[inputObj.id](parseFloat(inputObj.value));
    if(output != null) inputObj.value = clamp(output,inputObj.min,inputObj.max);
    
    if(inputObj.type == "checkbox") {
        inputObj.value = (inputObj.value > 0.0 ? 1.0 : 0.0);
        inputObj.checked = (inputObj.value > 0.0);
    }   
}

function __InjectInputCtrl(inputType,parentID,inputID,inputLabel,min,max,deltaStep,value,updateHandler,inputHandler,round)
{   
    deltaStep = "any";
      
    var html_code = "<label for='"+inputID+"'> ";
    if(inputLabel != null) html_code += inputLabel;
    html_code += " <input id='"+inputID+"' ";
    html_code += "type='"+inputType+"' ";
    if(min != null) html_code += "min='"+min+"' ";
    if(max != null) html_code += "max='"+max+"' ";
    if(deltaStep != null) html_code += "step='"+deltaStep+"' ";
    if(value != null) html_code += "value='"+value+"' ";
    html_code += "onchange='__inputEventHandler(this,"+round+");' style=\" ";
    if(inputType != "checkbox") html_code += "width:100%; height:100%; ";
    html_code += "margin:0; padding:0; border:none; text-align:center; border-radius:20px;\" /></label>";
            
    injectHTML(parentID,html_code);
    if(inputHandler  != null) __inputHandlerLookupTable[inputID] = inputHandler;
    if(updateHandler != null) __inputUpdaterLookupTable[inputID] = updateHandler;
}

function createInputCtrlInt(parentID,inputID,inputLabel,min,max,updateHandler,inputHandler)
{ 
    if(min == null) min = -Infinity;
    if(max == null) max = +Infinity;
    
    var value = ((updateHandler != null) ? updateHandler() : null);
    if(value == null) value = 0;
    
    min = Math.round(min);
    max = Math.round(max);
    value = clamp(value,min,max);
                                                                                                                              
    __InjectInputCtrl("number",parentID,inputID,inputLabel,min,max,1,value,updateHandler,inputHandler,true);
}

function createInputCtrlFloat(parentID,inputID,inputLabel,min,max,updateHandler,inputHandler)
{ 
    if(min == null) min = -Infinity;
    if(max == null) max = +Infinity;
    
    var value = ((updateHandler != null) ? updateHandler() : null);
    if(value == null) value = 0.0;
    value = clamp(value,min,max);
    
    var deltaStep = (abs(max-min)/100.0);
    if(deltaStep == Infinity) deltaStep = "any";
                                                                                                                                     
    __InjectInputCtrl("number",parentID,inputID,inputLabel,min,max,deltaStep,value,updateHandler,inputHandler,false);
}

function createInputCtrlBar(parentID,inputID,inputLabel,min,max,updateHandler,inputHandler)
{ 
    if(min == null) min = 0.0;
    if(max == null) max = 1.0;
    
    var value = ((updateHandler != null) ? updateHandler() : null);
    if(value == null) value = min;
    value = clamp(value,min,max);
                                                                                                                           
    __InjectInputCtrl("range",parentID,inputID,inputLabel,min,max,"any",value,updateHandler,inputHandler,false);
}

function createInputCtrlCheckBox(parentID,inputID,inputLabel,updateHandler,inputHandler)
{ 
    var value = ((updateHandler != null) ? (updateHandler()>0) : null);
    if(value == null) value = false;
                                                                                                                           
    __InjectInputCtrl("checkbox",parentID,inputID,inputLabel,0.0,1.0,null,value,updateHandler,inputHandler,false);
}

var __dialogItemID = 0;
    
function __injectDialogEntry(parent,itemID,label)
{   
    var code = "<div class='toolboxEntry unselectable' style=\"width:65%;\">";
    code += label+"</div><div id='dlg_item_"+itemID+"' class='toolboxEntry' style='width:35%;'></div>";

    injectHTML(parent,code);
}

function createDialogEntryInt(parentID,label,min,max,updateHandler,callback)
{
    __injectDialogEntry(parentID,__dialogItemID,label);
    createInputCtrlInt("dlg_item_"+__dialogItemID,"dlg_item_"+__dialogItemID+"_input",null,min,max,updateHandler,callback);
    
    return "dlg_item_"+__dialogItemID++ +"_input";   
}

function createDialogEntryFloat(parentID,label,min,max,updateHandler,callback)
{
    __injectDialogEntry(parentID,__dialogItemID,label);
    createInputCtrlFloat("dlg_item_"+__dialogItemID,"dlg_item_"+__dialogItemID+"_input",null,min,max,updateHandler,callback);
    
    return "dlg_item_"+__dialogItemID++ +"_input";    
}

function createDialogEntryBar(parentID,label,min,max,updateHandler,callback)
{
    __injectDialogEntry(parentID,__dialogItemID,label);
    createInputCtrlBar("dlg_item_"+__dialogItemID,"dlg_item_"+__dialogItemID+"_input",null,min,max,updateHandler,callback);      
    
    return "dlg_item_"+__dialogItemID++ +"_input";
}

function createDialogEntryCheckBox(parentID,label,updateHandler,callback)
{
    __injectDialogEntry(parentID,__dialogItemID,label);
    createInputCtrlCheckBox("dlg_item_"+__dialogItemID,"dlg_item_"+__dialogItemID+"_input",null,updateHandler,callback);      
    
    return "dlg_item_"+__dialogItemID++ +"_input";
}

function createDialogEntryImagePicker(parentID,label,updateHandler,callback)
{
    var inputID = "dlg_item_"+__dialogItemID+"_input";
    var baseSrc = ((updateHandler != null) ? updateHandler() : null);
    if(baseSrc == null) baseSrc = "";
    
    var html_code = "<div class='toolboxEntry unselectable' style='width:65%; height:64px; line-height:64px;'>";
    if(label != null) html_code += label;
    html_code += "</div><div class='toolboxEntry' style='width:35%; height:64px;'>";
    html_code += "<img id='" + inputID + "' src = '" + baseSrc + "' style='width:64px; height:64px; border:1px solid white; background-image:url("+ patternsBackground + ");' onmouseover=\"this.style.border='2px dashed blue';\" onmouseout=\"this.style.border='1px solid white';\" onclick=\"var newSrc = prompt('FILE/URL:',__inputUpdaterLookupTable[this.id]()); if(newSrc != null){ var outSrc = __inputHandlerLookupTable[this.id](newSrc); if(outSrc != null) newSrc = outSrc; setImageSource(this,newSrc); }\"></div>";
    injectHTML(parentID,html_code);

    if(updateHandler != null) __inputUpdaterLookupTable[inputID] = updateHandler;
    if(callback != null) __inputHandlerLookupTable[inputID] = callback;
    
    return "dlg_item_"+__dialogItemID++ +"_input";
}

function setImageSource(img,src) {
    img.src = ((src != null && src.length > 0) ? src : emptyImageSRC);    
}

function setDialogEntrySelectorItems(selector,items,selectedIndex)
{
    selector.innerHTML = "";
    items.every(function(item)
    {
        selector.innerHTML += "<option>" + item + "</option>";
        return true;
    });
    
    if(selectedIndex != null) selector.selectedIndex = selectedIndex;
    selector.selectedIndex = clamp(selector.selectedIndex,0,items.length-1);
}

function createDialogEntrySelector(parentID,label,updateHandler,callback)
{
    var inputID = "dlg_item_"+__dialogItemID+"_input";
    
    var html_code = "<div class='toolboxEntry unselectable' style='width:50%;'>";
    if(label != null) html_code += label;
    html_code += "</div><div class='toolboxEntry' style='width:50%;'>";
    html_code += "<select id='" + inputID + "' style='width:100%; border:none; border-radius:20px; text-indent:4px;' onchange='var newIndex = __inputHandlerLookupTable[this.id](this.selectedIndex); if(newIndex != null) this.selectedIndex = newIndex;'\"></select></div>";
    injectHTML(parentID,html_code);
    
    var selectorData = updateHandler();
    setDialogEntrySelectorItems(getElement(inputID),selectorData[0],selectorData[1]);

    if(updateHandler != null) __inputUpdaterLookupTable[inputID] = updateHandler;
    if(callback != null) __inputHandlerLookupTable[inputID] = callback;
    
    return "dlg_item_"+__dialogItemID++ +"_input";
}
       
function createDialogEntryButton(parentID,label,callback)
{
    var inputID = "dlg_item_"+__dialogItemID+"_input";
    var html_code = "<button id='" + inputID + "' onclick=\"__inputHandlerLookupTable[this.id]();\" style='margin:0px 0px 4px 0px; float:left; width:100%;'>" + label + "</button>";    
    injectHTML(parentID,html_code);
    
    if(callback != null) __inputHandlerLookupTable[inputID] = callback;
    return "dlg_item_"+__dialogItemID++ +"_input";
}

function readFileData(e,callback)  // does not work in Safari 
{
    if(typeof FileReader == "undefined") return alert("ERROR: Unable to open file \"" + e.files[0].name + "\"\nSorry, it looks like your browser does not support \"FileReader()\"");
    
    var file = e.files[0];
    if(!file) return;

    var reader = new FileReader();
    reader.onload = function(e) {
        var contents = e.target.result;
        callback(file.name,contents);
    };
    
    reader.readAsText(file);
}

function downloadData(filename,data)
{
    var a = document.createElement("a"),
    file = new Blob([data]);
        
    if(!window.navigator.msSaveOrOpenBlob)
    {            
        a.href = URL.createObjectURL(file);
        a.download = filename;
                    
        document.body.appendChild(a);
        a.click(); 
                    
        setTimeout(function()
        {
            window.URL.revokeObjectURL(a.href);
            document.body.removeChild(a);  
        }, 0);
         
    } else window.navigator.msSaveOrOpenBlob(file,filename);
}

var __eventCounter = 0;
function createDialogEntryFileSelector(parentID,callback)
{
    var inputID = "dlg_item_"+__dialogItemID+"_input";
    var html_code = "<input type='file' id='" + inputID + "' onchange=\"readFileData(this,__inputHandlerLookupTable[this.id]); return false;\" style='width:100%; margin-bottom:4px;' \>";
    
    injectHTML(parentID,html_code);
    
    if(callback != null) __inputHandlerLookupTable[inputID] = callback;
    return "dlg_item_"+__dialogItemID++ +"_input";
}

document.write("<div style='position:fixed; top:-100000px; width:0px; height:0px; opacity:0.0;'><input type='file' id='__customFileSelector' onchange=\"readFileData(this,__inputHandlerLookupTable[this.id]); return false;\"/></div>");
function openFileSelector(callback)
{
    __inputHandlerLookupTable["__customFileSelector"] = callback;
    
    var elem = document.getElementById("__customFileSelector");
    var evt = document.createEvent("MouseEvents");
    evt.initEvent("click",true,false);
    elem.dispatchEvent(evt); 
}

function createDialogTitle(parentID,title) {
    injectHTML(parentID,"<div class='toolboxTitle unselectable'>"+title+"</div>");    
}

function createDialogSeparator(parentID) {
    injectHTML(parentID,"<div class='toolboxSeparator unselectable'></div>");    
}

function createDialogEmptySeparator(parentID) {
    injectHTML(parentID,"<div class='toolboxEmptySeparator unselectable'></div>");    
}

function updateDialogEntries()
{           
    for(var key in __inputUpdaterLookupTable)
    {
        var updatedInputValue = __inputUpdaterLookupTable[key]();
        
        if(updatedInputValue != null)
        {
            var inputObj = getElement(key);
            
            if(inputObj.tagName == "IMG") setImageSource(inputObj,updatedInputValue);
            else if(inputObj.tagName == "INPUT")
            {
                inputObj.value = updatedInputValue;
                if(inputObj.type == "checkbox")
                {
                    if(inputObj.value      == "true")  inputObj.value = 1.0;
                    else if(inputObj.value == "false") inputObj.value = 0.0;

                    inputObj.checked = (inputObj.value > 0.0 ? 1.0 : 0.0);
                } 
            }
            else if(inputObj.tagName == "SELECT") setDialogEntrySelectorItems(inputObj,updatedInputValue[0],updatedInputValue[1]);
        } 
    }
}
