//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var sceneFX = null;
var sceneHUDs = null;
     
var emitters = [];
var emittersHUD = [];
var enableEmittersHUD = true;

var selectedEmitter = null;
var selectedSource = null;

function getDefaultEmitterProgramFX()
{
    var source = "\nvar linearSpeed  = 550.0;  // propulsion units per second\n";
    source += "var angularSpeed = 300.0;  // steering degrees per second\n\n";
    source += "if(data.target != null)\n{\n";
    source += "    var emitterVec = vec2(data.target);\n";
    source += "    emitterVec.sub(fx_Position);\n";
    source += "    emitterVec.normalize();\n\n";
    source += "    if(fx_Velocity.dot(fx_Velocity) <= 0.0) fx_Velocity = vec2(0.0,1.0);\n";
    source += "    else fx_Velocity.normalize();\n\n";
    source += "    var targetAngle = emitterVec.slope(fx_Velocity) / max(fx_Dt,0.0001);\n";
    source += "    var torqueForce = clamp(targetAngle, -angularSpeed, +angularSpeed);\n\n";
    source += "    fx_Velocity.rotate(torqueForce * fx_Dt);\n";
    source += "    fx_Velocity.mult(linearSpeed);\n}\n";
    
    return source;
}

function setSceneZoom(s)
{
    sceneFX.setScaleFactor(s);
    sceneHUDs.setScaleFactor(s);
}

function getSceneZoom() {
    return sceneFX.getScaleFactor();
}       

function createDefaultSceneFX(path)
{
    sceneFX = new SceneFX();
    sceneFX.enableVSync(true);
    sceneFX.enableMotionBlur(true);
    sceneFX.enableImageResampling(true);
    sceneFX.setZ(-1.0);
     
    sceneHUDs = new SceneFX();
    sceneHUDs.pause(); // freeze HUD scene, updated in sceneFX setSimulation() to ensure context synchronization
    sceneHUDs.enableMotionBlur(false);
    sceneHUDs.enableImageResampling(true);
    sceneHUDs.setZ(sceneFX.getZ());
    
    setSceneZoom(1.0);
     
    var emitterFX = sceneFX.createEmitter(path);
    emitters.push(emitterFX); 

    if(path == null)
    {
        emitterFX.setProgram(getDefaultEmitterProgramFX());
        emitterFX.pos.x = random(0.0,clientWidth()-209.0);
        emitterFX.pos.y = random(0.0,clientHeight());
        emitterFX.fixedSlope = null;  
           
        emitterFX.setEmitterPattern(null);
        emitterFX.emitterPatternBias  = 0.0;
        emitterFX.emitterPatternTrimX = 0.0;
        emitterFX.emitterPatternTrimY = 0.0;            
        emitterFX.emitterPatternSize = 100.0;
        
        emitterFX.setFlarePattern("MediaResources/flare.png");
        emitterFX.flarePatternTrimX = 0.0;
        emitterFX.flarePatternTrimY = 0.0;
        emitterFX.flarePatternBias = 0.0;
        emitterFX.flarePatternSize = 250.0;
        emitterFX.flarePatternFlickeringExp = 0.5;  
         
        var fireFX = emitterFX.createSource();
        fireFX.label = "fire & smoke";
        
        fireFX.setParticlesPatterns("MediaResources/fire.png","MediaResources/smoke.png");
        fireFX.particlesBlendEquationOut = BLEND_EQUATION.SUBTRACTIVE;
        fireFX.particlesBlendEquationIn = BLEND_EQUATION.SUBTRACTIVE;
        
        fireFX.delay = 0.0;
        fireFX.loop = false;
        fireFX.life = Infinity;
    
        fireFX.spawningTimeMin = 0.02;
        fireFX.spawningTimeMax = 0.02;
        fireFX.spawningAmountMin = 1.0;
        fireFX.spawningAmountMax = 1.0;
        
        fireFX.spanMin.x = fireFX.spanMax.x = 0.0;
        fireFX.spanMin.y = fireFX.spanMax.y = 0.0;
    
        fireFX.particlesLifeMin = 2.0;
        fireFX.particlesLifeMax = 2.0;
        
        fireFX.particlesDecayExp = 0.5;
        fireFX.particlesTransitionStartExp = 0.125;
        fireFX.particlesTransitionFadeExp = 0.4;
                        
        fireFX.particlesSpeedMin = 0.0;
        fireFX.particlesSpeedMax = 35.0;
        
        fireFX.particlesVelocityBiasMin = -180.0;
        fireFX.particlesVelocityBiasMax = +180.0;
        
        fireFX.particlesTorqueMin = -75.0;
        fireFX.particlesTorqueMax = +75.0;
        
        fireFX.particlesRotationMin = -180.0;
        fireFX.particlesRotationMax = +180.0;
        
        fireFX.particlesSizeMin = 7.0;
        fireFX.particlesSizeMax = 14.0;
        
        fireFX.particlesSizeVariationMin = 5.334;
        fireFX.particlesSizeVariationMax = 6.668;
    
        fireFX.particlesInertiaExp = 0.2;
        fireFX.particlesFrictionExp = 5.0;
        fireFX.particlesTorqueFrictionExp = 2.0;
        
        fireFX.particlesAcceleration.x = 0.0;
        fireFX.particlesAcceleration.y = 0.5;  
        
        var sparksFX = emitterFX.createSource();
        sparksFX.label = "sparks";
    
        sparksFX.setParticleProgram("fx_Slope = 0.0;");
        sparksFX.setParticlesPatterns("MediaResources/flare.png");
        sparksFX.particlesBlendEquationIn = BLEND_EQUATION.ADDITIVE;
        sparksFX.particlesBlendEquationOut = BLEND_EQUATION.ADDITIVE;
        
        sparksFX.delay = 0.0;
        sparksFX.loop = false;
        sparksFX.life = Infinity;
        
        sparksFX.spawningTimeMin = 0.01;
        sparksFX.spawningTimeMax = 0.02;
        sparksFX.spawningAmountMin = 0;
        sparksFX.spawningAmountMax = 1;
        
        sparksFX.spanMin.x = sparksFX.spanMax.x = 0.0;
        sparksFX.spanMin.y = sparksFX.spanMax.y = 0.0;
        
        sparksFX.particlesLifeMin = 1.0;
        sparksFX.particlesLifeMax = 2.0;
        
        sparksFX.particlesDecayExp = 0.25;
        sparksFX.particlesTransitionStartExp = 0.25;
        sparksFX.particlesTransitionFadeExp = 0.2;
                        
        sparksFX.particlesSpeedMin = 0.0;
        sparksFX.particlesSpeedMax = 150.0;
        
        sparksFX.particlesVelocityBiasMin = 130.0;
        sparksFX.particlesVelocityBiasMax = 230.0;
        
        sparksFX.particlesTorqueMin = 0.0;
        sparksFX.particlesTorqueMax = 0.0;
        
        sparksFX.particlesRotationMin = 0.0;
        sparksFX.particlesRotationMax = 0.0;
        
        sparksFX.particlesSizeMin = 10.0;
        sparksFX.particlesSizeMax = 25.0;
        
        sparksFX.particlesSizeVariationMin = 0.0;
        sparksFX.particlesSizeVariationMax = 0.0;
    
        sparksFX.particlesInertiaExp = 0.25;
        sparksFX.particlesFrictionExp = 10.0;
        sparksFX.particlesTorqueFrictionExp = 0.0;
        
        sparksFX.particlesAcceleration.x = 0.0;
        sparksFX.particlesAcceleration.y = 2.5;
        
        var flameCoreFX = emitterFX.createSource();
        flameCoreFX.label = "core";
     
        flameCoreFX.setParticlesPatterns("MediaResources/spark.png");
        flameCoreFX.particlesBlendEquationIn = BLEND_EQUATION.ADDITIVE;
        flameCoreFX.particlesBlendEquationOut = BLEND_EQUATION.ADDITIVE;
        
        flameCoreFX.delay = 0.0;
        flameCoreFX.loop = false;
        flameCoreFX.life = Infinity;
        
        flameCoreFX.spawningTimeMin = 0.02;
        flameCoreFX.spawningTimeMax = 0.02;
        flameCoreFX.spawningAmountMin = 1;
        flameCoreFX.spawningAmountMax = 1;
        
        flameCoreFX.spanMin.x = flameCoreFX.spanMax.x = 0.0;
        flameCoreFX.spanMin.y = flameCoreFX.spanMax.y = 0.0;
        
        flameCoreFX.particlesLifeMin = 0.5;
        flameCoreFX.particlesLifeMax = 0.5;
        
        flameCoreFX.particlesDecayExp = 0.25;
        flameCoreFX.particlesTransitionStartExp = 0.25;
        flameCoreFX.particlesTransitionFadeExp = 0.2;
                        
        flameCoreFX.particlesSpeedMin = 0.0;
        flameCoreFX.particlesSpeedMax = 150.0;
        
        flameCoreFX.particlesVelocityBiasMin = 130.0;
        flameCoreFX.particlesVelocityBiasMax = 230.0;
        
        flameCoreFX.particlesTorqueMin = 0.0;
        flameCoreFX.particlesTorqueMax = 0.0;
        
        flameCoreFX.particlesRotationMin = -180.0;
        flameCoreFX.particlesRotationMax = +180.0;
        
        flameCoreFX.particlesSizeMin = 15.0;
        flameCoreFX.particlesSizeMax = 25.0;
        
        flameCoreFX.particlesSizeVariationMin = 0.0;
        flameCoreFX.particlesSizeVariationMax = 0.0;
    
        flameCoreFX.particlesInertiaExp = 0.25;
        flameCoreFX.particlesFrictionExp = 10.0;
        flameCoreFX.particlesTorqueFrictionExp = 0.0;
        
        flameCoreFX.particlesAcceleration.x = 0.0;
        flameCoreFX.particlesAcceleration.y = 0.0;
    }
}

function updateEmittersHUD()
{ 
    for(var i=0; i<emittersHUD.length; ++i) sceneHUDs.deleteEmitter(emittersHUD[i]);
    emittersHUD = [];

    if(enableEmittersHUD)
    {
        for(var i=0; i<emitters.length; ++i)
        {
            var hud = sceneHUDs.createEmitter();
            emittersHUD.push(hud);
            
            hud.setEmitterPattern("MediaResources/compass_top.png");
            hud.setFlarePattern("MediaResources/compass_base.png");
            hud.flarePatternFlickeringExp = 0.0;           
            

            hud.setSimulation( function(thisHUD,dt)
            {
                var index = emittersHUD.indexOf(thisHUD);
                if(index < 0) return;
                
                thisHUD.pos.x = emitters[index].pos.x;
                thisHUD.pos.y = emitters[index].pos.y;
                thisHUD.fixedSlope = emitters[index].slope;

                thisHUD.flarePatternSize   = 90.0 / sceneHUDs.getScaleFactor();
                thisHUD.emitterPatternSize = 80.0 / sceneHUDs.getScaleFactor();
            });
        }
    }
}

function updateSampleEmitters(n)
{
    var baseEmitter = emitters[0];
    if(n == null) n = emitters.length;
    
    for(var i = emitters.length-1; i>0; --i) {
        sceneFX.deleteEmitter(emitters[i]);
        emitters.pop();
    }
   
    for(var i=0; i<n-1; ++i)
    {
        var newEmitter = sceneFX.createEmitterCopy(baseEmitter);
       
        newEmitter.pos.x = newEmitter.oldPos.x = random(0.0,sceneFX.getWidth());
        newEmitter.pos.y = newEmitter.oldPos.y = random(0.0,sceneFX.getHeight());
        emitters.push(newEmitter);
    }
    
    updateEmittersHUD();
}
