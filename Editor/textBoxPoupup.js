//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

var popupConsoleOnFocus = false;
var popupConsoleLastContent = null;
var popupConsoleActionHandler = null;
var popupConsoleStyle = getStyle("popupConsoleContainer");
var popupConsoleInputArea = getElement("popupConsoleTexArea");
    
function setPopupConsoleContent(content) {
    popupConsoleInputArea.value = popupConsoleLastContent = content;
}

function getPopupConsoleContent() {
    return popupConsoleInputArea.value;
}

function enablePopupConsoleTabSpaceRecognition() 
{
    popupConsoleInputArea.onkeydown = function(e)
    {
        if(e.keyCode == 9 || e.which == 9)
        {
            e.preventDefault();
            
            var tabSpace = "    ";             
            var s = this.selectionStart;
            this.value = this.value.substring(0,this.selectionStart) + tabSpace + this.value.substring(this.selectionEnd);
            this.selectionEnd = s + tabSpace.length; 
        }
    }
}

function runPopupConsoleAutoresizer()
{
    var resizer = new Animation(function(dt) {
        popupConsoleInputArea.style.width  = document.documentElement.clientWidth  * 0.5 + "px"; // force textarea's width  to be 50vw respectively
        popupConsoleInputArea.style.height = document.documentElement.clientHeight * 0.5 + "px"; // force textarea's height to be 50vh respectively
    });
    
    resizer.run(100);
}

function initPopupConsole()
{
    setPopupConsoleContent("");
    closePopupConsole(false);
    
    runPopupConsoleAutoresizer();
    enablePopupConsoleTabSpaceRecognition(); // WARNING: undo/redo text history seems to bug in most browsers when multiple lines get tab-indented at once 
}

function openPopupConsole(animated,caption,content,actionHandler)
{
    var lastTick = 0.0;
    var popupConsoleOpacity = 0.95;
    var scaleFactor = (animated ? 0.0 : popupConsoleOpacity);
        
    function fadeIn()
    {
        var currTick = new Date().getTime();
        var dt = ((lastTick > 0.0) ? (currTick-lastTick)/1000.0 : 0.0);
        lastTick = currTick;
        
        loadIdentity(popupConsoleStyle);
        translateNrm(popupConsoleStyle,-0.5,-0.5);
        scaleFactor = min(scaleFactor+(8.0 * dt),popupConsoleOpacity);
        scale(popupConsoleStyle,scaleFactor,scaleFactor);
        popupConsoleStyle.opacity = scaleFactor;
         
        if(scaleFactor < popupConsoleOpacity) setTimeout(fadeIn,10);
    }
    
    closePopupConsole(false,function()
    {   
        getElement("popupConsoleCaption").innerHTML = caption;
        popupConsoleActionHandler = actionHandler;
        setPopupConsoleContent(content);
        fadeIn();
    }); 
}

function closePopupConsole(animated,completionHandler)
{   
    if(getPopupConsoleContent() != popupConsoleLastContent) {
        if(!confirm("There are uncommitted changes, do you want to discard them ?")) return;
        setPopupConsoleContent(popupConsoleLastContent);
    }
     
    var lastTick = 0.0;
    var scaleFactor = (animated ? 1.0 : 0.0);
    
    function fadeOut()
    {
        var currTick = new Date().getTime();
        var dt = ((lastTick > 0.0) ? (currTick-lastTick)/1000.0 : 0.0);
        lastTick = currTick;
        
        loadIdentity(popupConsoleStyle);
        translateNrm(popupConsoleStyle,-0.5,-0.5);
        scaleFactor = max(scaleFactor-(8.0 * dt),0.0);
        scale(popupConsoleStyle,scaleFactor,scaleFactor);
        popupConsoleStyle.opacity = scaleFactor;
         
        if(scaleFactor > 0.0) setTimeout(fadeOut,10);
        else if(completionHandler != null) completionHandler();
    }
    
    fadeOut();  
}

function killPopupConsole(animated,completionHandler)
{
    setPopupConsoleContent(getPopupConsoleContent());
    closePopupConsole(animated,completionHandler);
}
