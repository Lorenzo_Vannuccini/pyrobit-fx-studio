//
//  This file is part of PyroBit FX Studio,
//  � 2017 Lorenzo Vannuccini, blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
 
var emptyImageSRC = "MediaResources/empty.png";
var patternsBackground = "MediaResources/chess.jpg";

function confirmDiscard() {
    return confirm("Unsaved changes will be lost, do you wish to continue ?");
}
              
function createToolDialogUI()
{
    selectedEmitter = emitters[0];
    selectedSource = selectedEmitter.sources[0];

    createDialogTitle("tool_box","PyroBit Studio");
    createDialogEntryFileSelector("tool_box",function(fileName,fileContent)
    {
        if(!FX_validateFileHeader(fileContent)) return alert("ERROR: Selected file is not valid or corrupted");
        
        var sandbox = new Sandbox();
        sandbox.decodedFX = null;
        
        if(!sandbox.run("this.decodedFX = " + FX_fileData(fileContent) + ";",function(error) { alert("ERROR: Selected file is not valid or corrupted"); })) return;
        __FX_DATA_CACHE["imported_fx"] = sandbox.decodedFX;
        sandbox.clear();
        
        setSceneZoom(1.0);    
        selectedEmitter.load("imported_fx");
        selectedSource = selectedEmitter.sources[0];
        
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryButton("tool_box","Create New FX",function()
    {
        if(!confirmDiscard()) return;
        
        selectedEmitter.free();
        selectedSource = selectedEmitter.createSource();
        selectedSource.setParticlesPatterns("MediaResources/particle.png","MediaResources/particle.png");
        selectedSource.label = "untitled source";
        
        selectedEmitter.setProgram(getDefaultEmitterProgramFX());
        selectedEmitter.pos.x = clientWidth()*0.5-209.0;
        selectedEmitter.pos.y = clientHeight()* 0.5;
        selectedEmitter.fixedSlope = null;  
        
        updateSampleEmitters();
        updateDialogEntries();          
    });
    
    createDialogEntryButton("tool_box","Open Sample FXs",function() {
        if(confirmDiscard()) window.location = "https://www.blackravenproduction.com/portfolio/lorenzo_vannuccini/PyroBit_FX_Studio/Samples%20FXs%20Collection/";        
    });
    
    createDialogEntryButton("tool_box","Download Current FX",function()
    {
        var name = prompt("FX File Name","");
        if(name == null) return;
        
        if(name.length < 1) name = "untitled FX";

        var encodedFX = FX_fileHeader()+"__FX_DATA_CACHE[\"__cache_import_fx\"] = ";
        encodedFX += selectedEmitter.encodeJSON() + ";";
        
        downloadData(name+".fx",encodedFX);                
    });

    createDialogEntryInt("tool_box","Sample Emitters:",1,99,function(){ return emitters.length; },function(amount) {
        updateSampleEmitters(amount);
        updateDialogEntries();
    });
    
    createDialogEntryBar("tool_box","Emitters Zoom Exp:",0.0,0.9,function(){ return 1.0-getSceneZoom(); },function(x)
    {
       setSceneZoom(1.0-x);
       updateDialogEntries();
    });
    
    createDialogEntryCheckBox("tool_box","Emitters HUDs",function(){ return enableEmittersHUD; },function(x)
    {
        enableEmittersHUD = x;
        updateEmittersHUD();
    }); 
    
    createDialogEntryCheckBox("tool_box","Stencil Motion Blur",function(){ return (sceneFX.renderingContext.blurExp > 0.0); },function(flag) {
        sceneFX.enableMotionBlur(flag);
    });
    
    createDialogSeparator("tool_box");
    createDialogTitle("tool_box","Emitter's Fare Pattern");
    createDialogEntryImagePicker("tool_box","Flare Pattern:",function(){ return ((selectedEmitter.flarePatternImage != null && selectedEmitter.flarePatternImage.path.length>0) ? selectedEmitter.flarePatternImage.path : ""); },function(src)
    {
        selectedEmitter.setFlarePattern(src);
        updateSampleEmitters();
        updateDialogEntries();                                                                                       
    });
    
    createDialogEntryFloat("tool_box","Pattern Trim X:",-2.0,+2.0,function(){ return selectedEmitter.flarePatternTrimY; },function(x)
    {
        selectedEmitter.flarePatternTrimX = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Pattern Trim Y:",-2.0,+2.0,function(){ return selectedEmitter.flarePatternTrimY; },function(y)
    {
        selectedEmitter.flarePatternTrimY = y;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Flickering Exp:",0.0,1.0,function(){ return selectedEmitter.flarePatternFlickeringExp; },function(x)
    {
        selectedEmitter.flarePatternFlickeringExp = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
            
    createDialogEntryFloat("tool_box","Pattern Size:",0.0,null,function(){ return selectedEmitter.flarePatternSize; },function(x)
    {
        selectedEmitter.flarePatternSize = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Pattern Bias:",null,null,function(){ return selectedEmitter.flarePatternBias; },function(x)
    {
        selectedEmitter.flarePatternBias = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogTitle("tool_box","Emitter's Base Pattern");
    createDialogEntryImagePicker("tool_box","Base Pattern:",function(){ return ((selectedEmitter.emitterPatternImage != null && selectedEmitter.emitterPatternImage.path.length>0) ? selectedEmitter.emitterPatternImage.path : ""); },function(src)
    {
        selectedEmitter.setEmitterPattern(src);
        updateSampleEmitters();
        updateDialogEntries();                                                                                       
    });
    
    createDialogEntryFloat("tool_box","Pattern Trim X:",-2.0,+2.0,function(){ return selectedEmitter.emitterPatternTrimX; },function(x)
    {
        selectedEmitter.emitterPatternTrimX = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Pattern Trim Y:",-2.0,+2.0,function(){ return selectedEmitter.emitterPatternTrimY; },function(y)
    {
        selectedEmitter.emitterPatternTrimY = y;
        updateSampleEmitters();
        updateDialogEntries();
    });
            
    createDialogEntryFloat("tool_box","Pattern Size:",0.0,null,function(){ return selectedEmitter.emitterPatternSize; },function(x)
    {
        selectedEmitter.emitterPatternSize = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Pattern Bias:",null,null,function(){ return selectedEmitter.emitterPatternBias; },function(x)
    {
        selectedEmitter.emitterPatternBias = x;
        updateSampleEmitters();
        updateDialogEntries();
    });

    createDialogSeparator("tool_box");
    createDialogTitle("tool_box","Emitter's Program Fx");
    createDialogEntryButton("tool_box","Edit Emitter Program Fx",function()
    {
        var source = ((selectedEmitter.program != null) ? selectedEmitter.program.source : "");
        openPopupConsole(true,"Emitter Program Fx",source,function(content)
        {
            selectedEmitter.clear();
            
            if(content.length < 1) {
                selectedEmitter.setProgram(null); 
                setPopupConsoleContent(content);
                updateSampleEmitters();
                updateDialogEntries();
                return;
            }
            
            var program = new programFX("EMITTER_PROGRAM");
            program.setErrorHandler( function(err){ alert(err); });
            
            if(program.compile(content)) {
                selectedEmitter.activeProgramFX = true;
                selectedEmitter.setProgram(content);
                setPopupConsoleContent(content);
                updateSampleEmitters();
                updateDialogEntries();
            }
        });
    });
    
    createDialogEntryCheckBox("tool_box","Active Program Fx",function(){ return (selectedEmitter.activeProgramFX && selectedEmitter.program != null && selectedEmitter.program.isValid()); },function(x)
    {    
        selectedEmitter.activeProgramFX = !selectedEmitter.activeProgramFX;
        updateSampleEmitters();
        updateDialogEntries();                                             
    });
    
    createDialogSeparator("tool_box");
    createDialogTitle("tool_box","Emitter's Sources");
    
    createDialogEntrySelector("tool_box","Selected Source:",function(){ return [selectedEmitter.getSourcesNameList(),selectedEmitter.getSourceIndex(selectedSource)]; },function(selectedIndex)
    {
        selectedSource = selectedEmitter.sources[selectedIndex];
        updateDialogEntries();    
    });
    
    createDialogEntryButton("tool_box","Create New Source",function()
    {
        var name = prompt("New Source's Name","");
        if(name == null) return;
        
        if(name.length < 1) name = "untitled source";
        
        var newSource = selectedEmitter.createSource();
        newSource.setParticlesPatterns("MediaResources/particle.png","MediaResources/particle.png");
        newSource.label = name;
        
        selectedSource = selectedEmitter.sources[selectedEmitter.sources.length-1];
        updateSampleEmitters();
        updateDialogEntries(); 
    });
    
    createDialogEntryButton("tool_box","Import New Source",function()
    {
        openFileSelector(function(fileName,fileContent)
        {     
            if(!FX_validateFileHeader(fileContent)) return alert("ERROR: Selected file is not valid or corrupted");
            
            var sandbox = new Sandbox();
            sandbox.decodedFX = null;
        
            if(!sandbox.run("this.decodedFX = " + FX_fileData(fileContent) + ";",function(error) { alert("ERROR: Selected file is not valid or corrupted"); })) return;
            __FX_DATA_CACHE["imported_fx"] = sandbox.decodedFX;
            sandbox.clear();
                    
            var importedEmitter = new EmitterFX();
            importedEmitter.load("imported_fx");

            selectedEmitter.importSources(importedEmitter.sources);
            importedEmitter.free();
            
            selectedSource = selectedEmitter.sources[selectedEmitter.sources.length-1];
            updateSampleEmitters();
            updateDialogEntries();
        });       
    });
    
    createDialogEntryButton("tool_box","Export This Source",function()
    {
        var name = prompt("FX Souce Name",selectedSource.label);
        if(name == null) return;
        
        if(name.length < 1) name = "untitled source FX";
        
        var emitterFX = new EmitterFX();
        emitterFX.importSources([selectedSource]);
        
        var encodedFX = FX_fileHeader()+"__FX_DATA_CACHE[\"__cache_import_fx\"] = ";
        encodedFX += emitterFX.encodeJSON() + ";";
        emitterFX.free();
         
        downloadData(name+".fx",encodedFX);
    });
    
    createDialogEntryButton("tool_box","Rename This Source",function()
    {
        var name = prompt("Source Name",selectedSource.label);
        if(name == null) return;
        
        if(name.length < 1) name = "untitled source";
        selectedSource.label = name;
        updateDialogEntries();
    });
    
    createDialogEntryButton("tool_box","Delete This Source",function()
    {
        if(selectedEmitter.sources.length <= 1) return alert("Can't delete: there must be at least one source per emitter!");
        if(!confirm("Delete source \"" + selectedSource.label + "\" from this Emitter ?")) return;
        
        selectedEmitter.deleteSource(selectedSource);
        
        selectedSource = selectedEmitter.sources[selectedEmitter.sources.length-1];
        updateSampleEmitters();
        updateDialogEntries(); 
    });
        
    createDialogSeparator("tool_box");
    createDialogEntryInt("tool_box","Source Z-order:",0,null,function(){ return selectedEmitter.sources.indexOf(selectedSource); },function(x)
    {
        x = clamp(x,0,selectedEmitter.sources.length-1);
        selectedEmitter.setSourceZ(selectedSource,x);
        selectedSource = selectedEmitter.sources[x];
        
        updateSampleEmitters();
        updateDialogEntries();
        return x;
    });
   
    createDialogEntryFloat("tool_box","Source Delay:",0.0,null,function(){ return selectedSource.delay; },function(x)
    {
        selectedSource.delay = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Source Life:",-1.0,null,function(){ return (selectedSource.life != Infinity ? selectedSource.life : -1); },function(x)
    {
        if(x < 0.0) x = -1.0;
        selectedSource.life = (x < 0.0) ? Infinity : x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
        return x;
    });
    
    createDialogEntryCheckBox("tool_box","Source Loop",function(){ return selectedSource.loop; },function(x)
    {
        selectedSource.loop = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Spawn Freq Min:",0.0001,null,function(){ return selectedSource.spawningTimeMin; },function(x)
    {
        selectedSource.spawningTimeMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Spawn Freq Max:",0.0001,null,function(){ return selectedSource.spawningTimeMax; },function(x)
    {
        selectedSource.spawningTimeMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Spawn Amount Min:",0.0,null,function(){ return selectedSource.spawningAmountMin; },function(x)
    {
        x = clamp(x,0,9999);
        selectedSource.spawningAmountMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
         
        return x;
    });
    
    createDialogEntryFloat("tool_box","Spawn Amount Max:",0.0,null,function(){ return selectedSource.spawningAmountMax; },function(x)
    {
        x = clamp(x,0,9999);
        selectedSource.spawningAmountMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
        
        return x;
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Spawn spanX Min:",null,null,function(){ return selectedSource.spanMin.x; },function(x)
    {
        selectedSource.spanMin.x = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Spawn spanX Max:",null,null,function(){ return selectedSource.spanMax.x; },function(x)
    {
        selectedSource.spanMax.x = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Spawn spanY Min:",null,null,function(){ return selectedSource.spanMin.y; },function(y)
    {
        selectedSource.spanMin.y = y;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Spawn spanY Max:",null,null,function(){ return selectedSource.spanMax.y; },function(y)
    {
        selectedSource.spanMax.y = y;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogTitle("tool_box","Source's Program Fx");
    createDialogEntryButton("tool_box","Edit Source Program Fx",function()
    {
        var source = ((selectedSource.program != null) ? selectedSource.program.source : "");
        openPopupConsole(true,"[\"" + selectedSource.label +"\"] Source Program Fx",source,function(content)
        {
            selectedEmitter.clear();
            
            if(content.length < 1) {
                selectedSource.setProgram(null); 
                setPopupConsoleContent(content);
                updateSampleEmitters();
                updateDialogEntries();
                return;
            }
            
            var program = new programFX("SOURCE_PROGRAM");
            program.setErrorHandler( function(err){ alert(err); });
            
            if(program.compile(content)) {
                selectedSource.activeProgramFX = true;
                selectedSource.setProgram(content);
                setPopupConsoleContent(content);
                updateSampleEmitters();
                updateDialogEntries();
            }
        });
    });
    
    createDialogEntryCheckBox("tool_box","Active Program Fx",function(){ return (selectedSource.activeProgramFX && selectedSource.program != null && selectedSource.program.isValid()); },function(x)
    {    
        selectedSource.activeProgramFX = !selectedSource.activeProgramFX;
        updateSampleEmitters();
        updateDialogEntries();                                             
    });
        
    createDialogSeparator("tool_box");
    createDialogTitle("tool_box","Source's Particles");
        
    createDialogEntryImagePicker("tool_box","Particle In:",function(){ return ((selectedSource.particlesImageIn != null && selectedSource.particlesImageIn.path.length>0) ? selectedSource.particlesImageIn.path : ""); },function(src)
    {
        selectedSource.setParticlesPatternIn(src);
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
        
        return selectedSource.particlesImageIn.path;                                                                                       
    });
    
    var availableBlendEquations = [];
    var formattedBlendEquations = [];
    
    for(var entry in BLEND_EQUATION)
    {
        availableBlendEquations.push(entry);
        formattedBlendEquations.push("blend " + entry.toLowerCase());
    } 
    
    function getBlendEquationIndex(equation)
    {
        var index = 0;
        for(var entry in BLEND_EQUATION)
        {
           if(BLEND_EQUATION[entry] == equation) return index;
            ++index;
        } 

        return -1;
    }
    
    createDialogEntrySelector("tool_box","",function(){ return [formattedBlendEquations,getBlendEquationIndex(selectedSource.particlesBlendEquationIn)]; },function(selectedIndex)
    {
        selectedSource.particlesBlendEquationIn = BLEND_EQUATION[availableBlendEquations[selectedIndex]];
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();       
    });
    
    createDialogEmptySeparator("tool_box");
    createDialogEntryImagePicker("tool_box","Particle Out:",function(){ return ((selectedSource.particlesImageOut != null && selectedSource.particlesImageOut.path.length>0) ? selectedSource.particlesImageOut.path : ""); },function(src)
    {
        selectedSource.setParticlesPatternOut(src);
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
        
        return selectedSource.particlesImageOut.path;                                                                                        
    });
    
    createDialogEntrySelector("tool_box","",function(){ return [formattedBlendEquations,getBlendEquationIndex(selectedSource.particlesBlendEquationOut)]; },function(selectedIndex)
    {    
        selectedSource.particlesBlendEquationOut = BLEND_EQUATION[availableBlendEquations[selectedIndex]];
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();      
    });
        
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Life Time Min:",0.0,60.0,function(){ return selectedSource.particlesLifeMin; },function(x)
    {
        selectedSource.particlesLifeMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();    
    });
    
    createDialogEntryFloat("tool_box","Life Time Max:",0.0,60.0,function(){ return selectedSource.particlesLifeMax; },function(x)
    {
        selectedSource.particlesLifeMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryBar("tool_box","Decay Fade Exp:",0.0,1.0,function(){ return selectedSource.particlesDecayExp; },function(x) {
        selectedSource.particlesDecayExp = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryBar("tool_box","Transition Pivot:",0.0,1,function(){ return selectedSource.particlesTransitionStartExp; },function(x){
        selectedSource.particlesTransitionStartExp = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryBar("tool_box","Transition Fade Exp:",0.0,1,function(){ return selectedSource.particlesTransitionFadeExp; },function(x){
        selectedSource.particlesTransitionFadeExp = x;
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Initial Speed Min:",0.0,null,function(){ return selectedSource.particlesSpeedMin; },function(x){
        selectedSource.particlesSpeedMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Initial Speed Max:",0.0,null,function(){ return selectedSource.particlesSpeedMax; },function(x){
        selectedSource.particlesSpeedMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Velocity Bias Min:",null,null,function(){ return selectedSource.particlesVelocityBiasMin; },function(x){
        selectedSource.particlesVelocityBiasMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Velocity Bias Max:",null,null,function(){ return selectedSource.particlesVelocityBiasMax; },function(x){
        selectedSource.particlesVelocityBiasMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Constant Accel X:",null,null,function(){ return selectedSource.particlesAcceleration.x; },function(x){
        selectedSource.particlesAcceleration.x = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Constant Accel Y:",null,null,function(){ return selectedSource.particlesAcceleration.y; },function(y){
        selectedSource.particlesAcceleration.y = y;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Torque Force Min:",null,null,function(){ return selectedSource.particlesTorqueMin; },function(x){
        selectedSource.particlesTorqueMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Torque Force Max:",null,null,function(){ return selectedSource.particlesTorqueMax; },function(x){
        selectedSource.particlesTorqueMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Initial Rotation Min:",null,null,function(){ return selectedSource.particlesRotationMin; },function(x){
        selectedSource.particlesRotationMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries(); 
    });
    
    createDialogEntryFloat("tool_box","Initial Rotation Max:",null,null,function(){ return selectedSource.particlesRotationMax; },function(x){
        selectedSource.particlesRotationMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries(); 
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Initial Size Min:",0.0,null,function(){ return selectedSource.particlesSizeMin; },function(x){
        selectedSource.particlesSizeMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Initial Size Max:",0.0,null,function(){ return selectedSource.particlesSizeMax; },function(x){
        selectedSource.particlesSizeMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Size Variation Min:",0.0,null,function(){ return selectedSource.particlesSizeVariationMin; },function(x){
        selectedSource.particlesSizeVariationMin = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Size Variation Max:",0.0,null,function(){ return selectedSource.particlesSizeVariationMax; },function(x){
        selectedSource.particlesSizeVariationMax = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogSeparator("tool_box");
    createDialogEntryFloat("tool_box","Inertia Exp:",null,null,function(){ return selectedSource.particlesInertiaExp; },function(x){
        selectedSource.particlesInertiaExp = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Friction Exp:",0.0,null,function(){ return selectedSource.particlesFrictionExp; },function(x){
        selectedSource.particlesFrictionExp = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });
    
    createDialogEntryFloat("tool_box","Friction Torque Exp:",0.0,null,function(){ return selectedSource.particlesTorqueFrictionExp; },function(x){
        selectedSource.particlesTorqueFrictionExp = x;
        selectedEmitter.clear();
        updateSampleEmitters();
        updateDialogEntries();
    });

    createDialogSeparator("tool_box");
    createDialogTitle("tool_box","Particle's Program Fx");
    createDialogEntryButton("tool_box","Edit Particle Program Fx",function()
    {
        var source = ((selectedSource.particleProgram != null) ? selectedSource.particleProgram.source : "");
        openPopupConsole(true,"[\"" + selectedSource.label +"\"] Particle Program Fx",source,function(content)
        {
            selectedEmitter.clear();
            
            if(content.length < 1) {
                selectedSource.setParticleProgram(null); 
                setPopupConsoleContent(content);
                updateSampleEmitters();
                updateDialogEntries();
                return;
            }
            
            var program = new programFX("PARTICLE_PROGRAM");
            program.setErrorHandler( function(err){ alert(err); });
            
            if(program.compile(content)) {
                selectedSource.activeParticleProgramFX = true;
                selectedSource.setParticleProgram(content);
                setPopupConsoleContent(content);
                updateSampleEmitters();
                updateDialogEntries();
            }
        });
    });
    
    createDialogEntryCheckBox("tool_box","Active Program Fx",function(){ return (selectedSource.activeParticleProgramFX && selectedSource.particleProgram != null && selectedSource.particleProgram.isValid()); },function(x)
    {    
        selectedSource.activeParticleProgramFX = !selectedSource.activeParticleProgramFX;
        updateSampleEmitters();
        updateDialogEntries();                                             
    });
    
    updateDialogEntries();
    runFpsViewUpdater();
}

function runFpsViewUpdater()
{
    var viewUpdater = new Animation( function()
    {        
        setHTML("fps_counter","FPS: " + sceneFX.fps);
        setHTML("active_particles_counter","Active Particles: " + sceneFX.activeParticles);
        setHTML("cached_particles_counter","Cached Textures: " + (imageCacheFX.size - 2*enableEmittersHUD)); // omit HUD patterns from cache count    
    });
        
    viewUpdater.run(100);
}