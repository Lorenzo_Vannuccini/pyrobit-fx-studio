//
//  This file is part of PyroBit FX Studio,
//  created by Lorenzo Vannuccini: blackravenprod@gmail.com
//  
//  PyroBit FX Studio is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PyroBit FX Studio is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//

function loadIdentity(styleID)
{
    styleID.webkitTransform = "matrix3d(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1)";
    styleID.msTransform =     "matrix3d(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1)";    
}

function translate(styleID,x,y)
{
    styleID.webkitTransform += "translate(" + x + "px," + y +"px)";
    styleID.msTransform     += "translate(" + x + "px," + y +"px)";                
}

function translateNrm(styleID,x,y)
{
    var rateX = x * 100.0, rateY = y * 100.0;
    styleID.webkitTransform += "translate(" + rateX + "%," + rateY +"%)";
    styleID.msTransform     += "translate(" + rateX + "%," + rateY +"%)";                
}

function rotate(styleID,dgr)
{
    styleID.webkitTransform += "rotate(" + dgr + "deg)";
    styleID.msTransform += "rotate(" + dgr + "deg)";                
}

function scale(styleID,x,y)
{
    styleID.webkitTransform += "scale(" + x + "," + y +")";
    styleID.msTransform += "scale(" + x + "," + y +")";                
}

